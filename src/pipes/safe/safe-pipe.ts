import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {

    /**
     * Constructor.
     */
    constructor(private sanitizer: DomSanitizer) {}

    /**
     * Helper pipe to secure a given outgoing url. Use this whenever you need to link
     * somewhere outside of the app.
     *
     * @param {String} url
     *
     * @return DomSanitizer
     */
    transform(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
}