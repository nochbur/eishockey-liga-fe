import { PipeTransform, Pipe } from '@angular/core';

@Pipe({name: 'keys'})
export class KeysPipe implements PipeTransform {
    /**
     * Helper pipe to loop over an object within a template.
     *
     * @param {Object} value
     *
     * @return {Array}
     */
    transform(value) : any {
        let keys = [];
        for (let key in value) {
            keys.push({key: key, value: value[key]});
        }

        return keys;
    }
}