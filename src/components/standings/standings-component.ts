import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TeamPage } from '../../pages/team/team';

@Component({
    selector: 'standings-component',
    templateUrl: 'standings-component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class StandingsComponent {
    @Input()
    teams: Array<{}> = null;

    @Input()
    leagueConfig: Object = {};

    @Input()
    title: string = '';

    /**
     * @param {NavController} navCtrl
     */
    constructor(
        private navCtrl: NavController
    ) {
    }

    /**
     * Fetches all needed data for the page.
     */
    onOpenTeamPage(teamId) {
        this.navCtrl.push(TeamPage, {
            leagueConfig: this.leagueConfig,
            teamId: teamId
        });
    }
}