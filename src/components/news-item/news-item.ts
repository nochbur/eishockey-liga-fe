import { Component, Input } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { InAppBrowserInterface } from '../../utilities/in-app-browser-helper';

@Component({
    selector: 'news-item',
    templateUrl: 'news-item.html'
})

export class NewsItemComponent extends InAppBrowserInterface {
    @Input()
    item: Object = {};

    constructor(
        inAppBrowser: InAppBrowser
    ) {
        super(inAppBrowser);
    }

    /**
     * Redirects to the detail page of an item.
     */
    openNewsItem() {
        this.openUrlInAppBrowser(this.item['link']);
    }
}
