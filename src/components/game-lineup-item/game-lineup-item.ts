import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PlayerPage } from '../../pages/player/player';

@Component({
    selector: 'game-lineup-item',
    templateUrl: 'game-lineup-item.html'
})

export class GameLineupItemComponent {
    @Input()
	teamId: Number = null;

    @Input()
	item: Array<{}> = null;

    @Input()
	leagueId: Number = null;

    @Input()
	hasLinkToPlayerPage: Boolean = true;

    /**
     * Constructor.
     */
    constructor(
        public navCtrl: NavController
    ) {
    }

    /**
     * Redirects the user to a requested player page.
     */
    viewPlayer() {
        if (this.hasLinkToPlayerPage) {
            this.navCtrl.push(PlayerPage, {
                playerId: this.item['id'],
                leagueId: this.leagueId
            });
        }
    }
}
