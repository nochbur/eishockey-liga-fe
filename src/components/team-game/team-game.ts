import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GamePage } from '../../pages/game/game';

@Component({
    selector: 'team-game-component',
    templateUrl: 'team-game.html'
})

export class TeamGameComponent {
    @Input()
    game: Object = null;

    @Input()
    league: Object = null;

    /**
     * @param {navCtrl} NavController
     */
    constructor(
        private navCtrl: NavController
    ) {
    }

    /**
     * Redirects the user to a requested game page.
     */
    onViewGame() {
        this.navCtrl.push(GamePage, {
            gameId: this.game['id'],
            gameData: this.game,
            league: this.league
        });
    }
}