import { Component, Input } from '@angular/core';

@Component({
    selector: 'schedule-live-list',
    templateUrl: 'schedule-live-list.html'
})

export class ScheduleLiveListComponent {
    @Input()
    liveGames: Array<{}> = null;

    @Input()
    league: Object = null;
}