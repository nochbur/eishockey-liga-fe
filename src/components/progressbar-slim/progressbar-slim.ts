import { Component, Input } from '@angular/core';

@Component({
    selector: 'progressbar-slim',
    templateUrl: 'progressbar-slim.html'
})

export class ProgressbarSlimComponent {
    @Input()
    percentage: number = 0;

    @Input()
    leftFloater: boolean = true;

    /**
     * Called when scope input changes.
     *
     * @param {Object} changes
     */
    ngOnChanges(changes) {
        this.setPercentage();
    }

    /**
     * Sets the scope percentage value.
     */
    private setPercentage() {
        this.percentage = Math.round(this.percentage);
    }
}
