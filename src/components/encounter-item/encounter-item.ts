import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TeamPage } from '../../pages/team/team';

@Component({
    selector: 'encounter-item',
    templateUrl: 'encounter-item.html'
})

export class EncounterItemComponent {
    @Input()
    leagueConfig: Object = null;

    @Input()
    encounter: Object = null;

    @Input()
    isGameRunning: boolean = false;

    /**
     * @param {NavController} navCtrl
     */
    constructor(
        private navCtrl: NavController
    ) {
    }

    /**
     * Fetches all needed data for the page.
     */
    onOpenTeamPage(teamId) {
        this.navCtrl.push(TeamPage, {
            leagueConfig: this.leagueConfig,
            teamId: teamId
        });
    }
}