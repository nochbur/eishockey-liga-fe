import { Component, Input } from '@angular/core';

@Component({
    selector: 'custom-image',
    templateUrl: 'custom-image.html'
})

export class CustomImageComponent {
    @Input()
    src: String = null;

    @Input()
    fallbackType = 'default';

    fallbackUrls = {
        'default': '/assets/images/image-fallback.png',
        'member': '/assets/images/image-fallback-member.png'
    };

    /**
     * Updates the src of the image to the fallbackUrl in case of an error.
     * An error could occur if a given src URL returns a 404 status.
     */
    updateUrl(event) {
        this.src = this.fallbackUrls[this.fallbackType];
    }
}
