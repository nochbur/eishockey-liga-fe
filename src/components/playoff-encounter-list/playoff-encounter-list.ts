import { Component, Input } from '@angular/core';

@Component({
    selector: 'playoff-encounter-list',
    templateUrl: 'playoff-encounter-list.html'
})

export class PlayoffEncounterListComponent {
    @Input()
    encounter: Object = null;

    @Input()
    league: Object = null;
}