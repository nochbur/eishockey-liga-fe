import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { SimpleChanges } from '@angular/core';

@Component({
    selector: 'custom-table',
    templateUrl: 'custom-table.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class CustomTableComponent {
    @Input()
    title: string = '';

    @Input()
    lines: Array<Array<String|Number>> = [];

    @Input()
    columnNames: Array<String> = [];

    @Input()
    columnWidth: Array<Number> = [];

    @Input()
    hasSeparator: Boolean = true;

    isValid: Boolean = false;

    hasHeaderText: Boolean = false;

    /**
     * Handle changing of input variables.
     */
    ngOnChanges(changes: SimpleChanges) {
        this.validateInput();
        this.handleHeaderText();
    }

    /**
     * Validates the given Input.
     */
    validateInput() {
        if (this.lines.length === 0) {
            console.error('No lines given to create table.');

            return;
        }

        if (this.columnNames.length === 0) {
            console.error('No column names given to create table.');

            return;
        }

        if (this.columnWidth.length === 0) {
            console.error('No column width given to create table.');

            return;
        }

        if (this.columnNames.length !== this.columnWidth.length) {
            console.error('Count of names and width of table is different.');

            return;
        }

        this.isValid = true;

        return;
	}

    /**
     * Checks wheter any of the columnNames is not an empty string.
     */
    handleHeaderText() {
        let i = 0;
        while(i < this.columnNames.length) {
            if (this.columnNames[i].length > 0) {
                this.hasHeaderText = true;

                break;
            }

            i++;
        }
    }
}
