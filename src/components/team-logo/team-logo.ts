import { Component, Input } from '@angular/core';
import { HockeyDataConfigService } from '../../services/api';

@Component({
    selector: 'team-logo',
    templateUrl: 'team-logo.html'
})

export class TeamLogoComponent {
    @Input()
    teamId: Number = null;

    fallbackUrl: string = '/assets/images/image-fallback.png';
    teamLogoBaseUrl: string = '';

    /**
     * Constructor.
     */
    constructor(
        private hockeyDataConfig: HockeyDataConfigService
    ) {
        this.teamLogoBaseUrl = this.hockeyDataConfig.retrieveApiBaseUrl() + '/images/teams/';
    }
}
