import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PlayerPage } from '../../pages/player/player';

@Component({
    selector: 'team-roster-component',
    templateUrl: 'team-roster.html'
})

export class TeamRosterComponent {
    @Input()
    roster: Object = null;

    @Input()
	league: Object = null;

    /**
     * Constructor.
     */
    constructor(
        public navCtrl: NavController
    ) {
    }

    /**
     * Redirects the user to a requested player page.
     *
     * @param {Object} player
     */
    viewPlayer(player) {
        this.navCtrl.push(PlayerPage, {
            playerId: player['id'],
            leagueId: this.league['id']
        });
    }
}
