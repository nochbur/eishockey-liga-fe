import { find } from 'lodash';
import { Component, Input } from '@angular/core';

@Component({
    selector: 'team-statistics-component',
    templateUrl: 'team-statistics.html'
})

export class TeamStatisticsComponent {
    private HEX_COLOR_RED = '#787F8C';
    private HEX_COLOR_GREY = '#F02643';
    private HEX_COLOR_BLUE = '#121824';

    @Input()
    team = null;

    teamStandings = null;
    gamesChart = null;
    playoffChart: Object = null;
    penaltiesChart: Object = null;

   /**
     * Triggered when input data changes.
     *
     * @param {Object} changes
     */
    ngOnChanges(changes) {
        this.teamStandings = find(this.team.standings.preliminary, { id: this.team.information.id });

        this.gamesChart = this.getDoughnutChart([
            this.team.statistics.gamesPlayed,
            this.team.statistics.gamesTotal
        ]);

        this.gamesChart.stats = {
            'totalGamesWon': this.getTotalGamesWon(),
            'totalGamesLost': this.getTotalGamesLost(),
        }

        this.playoffChart = this.getDoughnutChart([
            this.team.statistics.powerplayGoals,
            this.team.statistics.powerplays
        ]);

        this.penaltiesChart = this.getDoughnutChart([
            this.team.statistics.penalties,
            this.team.statistics.penaltyGoals
        ]);
    }

   /**
     * Returns a percentage of the given input values.
     *
     * @param {Number} max
     * @param {Number} current
     *
     * @return {Number}
     */
    doPercentageCalculation(max, current) {
        if (typeof max === 'number' && typeof current === 'number') {
            let result = Math.round((current * 100) / max);

            if (typeof result === 'number') {
                return result;
            }
        }

        return 0;
    }

   /**
     * Returns a chart data object.
     *
     * @param {Array} data
     *
     * @return {Object}
     */
    private getDoughnutChart(data) {
        return {
            type: 'doughnut',
            stats: {},
            data: {
                labels: [],
                datasets: [
                    {
                        label: '',
                        data: data,
                        backgroundColor: [
                            this.HEX_COLOR_GREY,
                            this.HEX_COLOR_RED
                        ],
                        borderWidth: 1,
                        borderColor: this.HEX_COLOR_BLUE,
                        hoverBackgroundColor: false,
                        hoverBorderColor: false,
                        hoverBorderWidth: 0
                    }
                ]
            },
            options: {
                dynamicDisplay : true,
                responsive: true,
                maintainAspectRatio: false,
                tooltips: {
                    enabled: false
                },
                cutoutPercentage: 90
            }
        };
    }

   /**
     * Returns the count of the won games.
     *
     * @return {Number}
     */
    private getTotalGamesWon() {
        // TODO Make this more generic.
        return this.teamStandings.gamesWon + this.teamStandings.gamesWonInOt;
    }

   /**
     * Returns the count of the lost games.
     *
     * @return {Number}
     */
    private getTotalGamesLost() {
        // TODO Make this more generic.
        return this.teamStandings.gamesLost + this.teamStandings.gamesLostInOt;
    }
}
