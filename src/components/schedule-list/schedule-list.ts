import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { DateHelper } from '../../utilities/date-helper';

@Component({
    selector: 'schedule-list',
    templateUrl: 'schedule-list.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ScheduleList {
    @Input()
    games: Array<{}> = null;

    @Input()
    league: Object = null;

    @Input()
    showGameDate: boolean = false;

    @Input()
    hideBoxShadow: boolean = false;

    /**
     * @param {dateHelper} DateHelper
     */
    constructor(public dateHelper: DateHelper) {}

    /**
     * Improves the performace of a ng-repeat.
     *
     * @param {Number} index
     *
     * @return {Number}
     */
    trackGameDay(index) {
        return index;
    }

    /**
     * Improves the performace of a ng-repeat.
     *
     * @param {Number} index
     * @param {Object} game
     *
     * @return {Number|undefined}
     */
    trackGame(index, game) {
        return game ? game.id : undefined;
    }

    /**
     * TODO: move to general handler.
     * Returns a formatted date string.
     *
     * @param {Datetime} date
     *
     * @return {String}
     */
    getFormattedGameDay(date) {
        if (!date) {
            return '';
        }

        let formattedDate = this.dateHelper.getMomentObj(date, 'DD.MM.YYYY');

        if (!formattedDate.isValid()) {
            return '';
        }

        return formattedDate.format('dddd, DD.MM.YYYY');;
    }
}
