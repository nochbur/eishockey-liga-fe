import { Component } from '@angular/core';
import { NewsService } from '../../services/api';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { InAppBrowserInterface } from '../../utilities/in-app-browser-helper';

@Component({
    selector: 'news-slider',
    templateUrl: 'news-slider.html'
})

export class NewsSlider extends InAppBrowserInterface {

    news: Array<{}> = [];

    /**
     * Constructor.
     */
    constructor(
        inAppBrowser: InAppBrowser,
        private newsService: NewsService
    ) {
        super(inAppBrowser);
        this.fetchFromApi();
    }

    /**
     * Fetches the latest news.
     */
    fetchFromApi() {
        this.newsService.retrieveNews()
            .subscribe(
                data => this.onNewsFetched(data, null),
                error => this.onNewsFetched(null, error)
            );
    }

    /**
     * Triggered when the news are fetched.
     *
     * @param {Object|null} data
     * @param {Object|null} error
     */
    onNewsFetched(data, error) {
        if (error) {
            return false;
        }

        if (!data['news']) {
            return false;
        }

        this.news = data['news']['AT'] || [];

        if (this.news.length > 5) {
            this.news = this.news.splice(0, 5);
        }
    }

    /**
     * Redirects to the detail page of an item.
     */
    openNewsItem(item) {
        this.openUrlInAppBrowser(item['link']);
    }
}
