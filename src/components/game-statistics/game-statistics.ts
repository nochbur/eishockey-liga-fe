import { Component, Input } from '@angular/core';

@Component({
    selector: 'game-statistics',
    templateUrl: 'game-statistics.html'
})

export class GameStatisticsComponent {
    @Input()
    gameStatistics: Object = null;

    @Input()
    gameOfficials: Object = null;

    homeShootsOnGoalPercentage: number = 0;
    awayShootsOnGoalPercentage: number = 0;

    homeTimeLeadingPercentage: number = 0;
    awayTimeLeadingPercentage: number = 0;

    homeTimeTiedPercentage: number = 0;
    awayTimeTiedPercentage: number = 0;

    homeTimeTrailingPercentage: number = 0;
    awayTimeTrailingPercentage: number = 0;

    homePenaltiesPercentage: number = 0;
    awayPenaltiesPercentage: number = 0;

    homePowerplayGoalsPercentage: number = 0;
    awayPowerplayGoalsPercentage: number = 0;

    homePowerplayPercentage: number = 0;
    awayPowerplayPercentage: number = 0;

    homePenaltykillingPercentage: number = 0;
    awayPenaltykillingPercentage: number = 0;

    homeTimeLeadingString: string = '';
    awayTimeLeadingString: string = '';

    homeTimeTiedString: string = '';
    awayTimeTiedString: string = '';

    homeTimeTrailingString: string = '';
    awayTimeTrailingString: string = '';

    /**
     * Triggered when the input values change.
     *
     * @param {Object}
     */
    ngOnChanges(changes) {
        this.buildPercentages();
        this.buildTimes();
    }

    /**
     * Formats the time strings.
     */
    private buildTimes() {
        this.homeTimeLeadingString = this.retrieveDateString(this.gameStatistics['homeTimeLeading']);
        this.awayTimeLeadingString = this.retrieveDateString(this.gameStatistics['awayTimeLeading']);

        this.homeTimeTiedString = this.retrieveDateString(this.gameStatistics['homeTimeTied']);
        this.awayTimeTiedString = this.retrieveDateString(this.gameStatistics['awayTimeTied']);

        this.homeTimeTrailingString = this.retrieveDateString(this.gameStatistics['homeTimeTrailing']);
        this.awayTimeTrailingString = this.retrieveDateString(this.gameStatistics['awayTimeTrailing']);
    }

    /**
     * Calculates all percentages.
     */
    private buildPercentages() {
        let max = this.gameStatistics['homeShotsOnGoal'] + this.gameStatistics['awayShotsOnGoal'];
        this.homeShootsOnGoalPercentage = this.doPercentageCalculation(max, this.gameStatistics['homeShotsOnGoal']);
        this.awayShootsOnGoalPercentage = this.doPercentageCalculation(max, this.gameStatistics['awayShotsOnGoal']);

        if (this.gameStatistics['numberOfPeriods']
            && this.gameStatistics['lengthOfPeriod']
            && this.gameStatistics['numberOfOvertimes']
            && this.gameStatistics['lengthOfOvertime']) {
            max = (this.gameStatistics['numberOfPeriods'] * this.gameStatistics['lengthOfPeriod'])
                + (this.gameStatistics['numberOfOvertimes'] * this.gameStatistics['lengthOfOvertime']);
        } else {
            max = 3600;
        }
        this.homeTimeLeadingPercentage = this.doPercentageCalculation(max, this.gameStatistics['homeTimeLeading']);
        this.awayTimeLeadingPercentage = this.doPercentageCalculation(max, this.gameStatistics['awayTimeLeading']);

        this.homeTimeTiedPercentage = this.doPercentageCalculation(max, this.gameStatistics['homeTimeTied']);
        this.awayTimeTiedPercentage = this.doPercentageCalculation(max, this.gameStatistics['awayTimeTied']);

        this.homeTimeTrailingPercentage = this.doPercentageCalculation(max, this.gameStatistics['homeTimeTrailing']);
        this.awayTimeTrailingPercentage = this.doPercentageCalculation(max, this.gameStatistics['awayTimeTrailing']);

        max = this.gameStatistics['homePenalties'] + this.gameStatistics['awayPenalties'];
        this.homePenaltiesPercentage = this.doPercentageCalculation(max, this.gameStatistics['homePenalties']);
        this.awayPenaltiesPercentage = this.doPercentageCalculation(max, this.gameStatistics['awayPenalties']);

        max = this.gameStatistics['awayPenalties'];
        this.homePowerplayGoalsPercentage = this.doPercentageCalculation(max, this.gameStatistics['homePowerplayGoals']);
        max = this.gameStatistics['homePenalties'];
        this.awayPowerplayGoalsPercentage = this.doPercentageCalculation(max, this.gameStatistics['awayPowerplayGoals']);

        this.homePowerplayPercentage = this.doPercentageCalculation(this.gameStatistics['awayPenalties'], this.gameStatistics['homePowerplayGoals']);
        this.awayPowerplayPercentage = this.doPercentageCalculation(this.gameStatistics['homePenalties'], this.gameStatistics['awayPowerplayGoals']);

        this.homePenaltykillingPercentage = 100 - this.awayPowerplayPercentage;
        this.awayPenaltykillingPercentage = 100 - this.homePowerplayPercentage;
    }

    /**
     * Returns a percentage of the given input values.
     *
     * @param {Number} max
     * @param {Number} current
     *
     * @return {Number}
     */
    private doPercentageCalculation(max, current) {
        let defaultResult = 0;

        if (typeof max === 'number' && typeof current === 'number') {
            let result = Math.round((current * 100) / max);

            if (typeof result === 'number') {
                return result;
            }
        }

        return defaultResult;
    }

    /**
     * Returns a string for minutes and seconds.
     *
     * @param {Number} seconds
     *
     * @return {String}
     */
    private retrieveDateString(seconds) {
        let result = '';
        let minutes = Math.floor(parseInt(seconds, 10) / 60);
        let sec = parseInt(seconds, 10) - (minutes * 60);

        if (minutes < 10) {
            result += '0';
        }

        result += minutes + ':';

        if (sec < 10) {
            result += '0';
        }

        result += sec;

        return result;
    }
}