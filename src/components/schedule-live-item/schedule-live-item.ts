import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GamePage } from '../../pages/game/game';

@Component({
    selector: 'schedule-live-item',
    templateUrl: 'schedule-live-item.html'
})

export class ScheduleLiveItemComponent {
    @Input()
    encounter: Object = null;

    @Input()
    league: Object = null;

    homeTeam: Object = null;
    awayTeam: Object = null;

    isGameRunning: boolean = false;
    isRegular: boolean = false;
    isOvertime: boolean = false;
    isShootout: boolean = false;
    isFinished: boolean = false;

    /**
     * Constructor.
     */
    constructor(public navCtrl: NavController) {}

    /**
     * Triggered when input data change.
     *
     * @param {Object} changes
     */
    ngOnChanges(changes) {
        this.buildEncounterData();
        this.checkGameStatus();
    }

    /**
     * Builds the scope parameters needed to display the encounter item.
     */
    buildEncounterData() {
        this.homeTeam = {
            'id': this.encounter['homeTeamId'],
            'name': this.encounter['homeTeamLongName']
        };
        this.awayTeam = {
            'id': this.encounter['awayTeamId'],
            'name': this.encounter['awayTeamLongName']
        };
    }

    /**
     * Checks if the current game is still running or not.
     *
     * return {Boolean}
     */
    checkGameStatus() {
        this.isGameRunning = false;
        this.isRegular = false;
        this.isOvertime = false;
        this.isShootout = false;
        this.isFinished = false;

        if (this.encounter['gameHasEnded']) {
            this.isFinished = true;

            return true;
        }

        let today = new Date();
        let hh = this.checkTime(today.getHours());
        let mm = this.checkTime(today.getMinutes());
        let time = hh + ':' + mm;

        if (this.encounter['gameStatus'] > 0 && time >= this.encounter['scheduledTime']) {
            this.isGameRunning = true;

            if (this.encounter['isOvertime']) {
                this.isOvertime = true;
            } else if (this.encounter['isShootOut']) {
                this.isShootout = true;
            } else {
                this.isRegular = true;
            }
        }

        return true;
    }

    /**
     * Redirects to a game detail page.
     */
    viewGame() {
        this.navCtrl.push(GamePage, {
            gameId: this.encounter['id'],
            gameData: this.encounter,
            league: this.league
        });
    }

    /**
     * Formats a given time string.
     *
     * @param {String} time
     *
     * @return {String}
     */
    checkTime(time) {
        if (time < 10) {
            time = "0" + time;
        }
        return time;
    }
}