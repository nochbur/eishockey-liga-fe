import { Component, Input } from '@angular/core';

@Component({
    selector: 'game-lineup',
    templateUrl: 'game-lineup.html'
})

export class GameLineupComponent {
    @Input()
    headline: string = '';

    @Input()
    homeId: string = '';

    @Input()
    homeMember: Array<{}> = null;

    @Input()
    awayId: string = '';

    @Input()
    awayMember: Array<{}> = null;

    @Input()
    leagueId: Number = null;

    @Input()
    hasLinkToPlayerPage: Boolean = true;
}
