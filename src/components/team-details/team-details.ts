import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PlayerPage } from '../../pages/player/player';

@Component({
    selector: 'team-details-component',
    templateUrl: 'team-details.html'
})

export class TeamDetailsComponent {
    @Input()
    games: Object = null;

    @Input()
    league: Object = null;

    @Input()
    topscorer: Array<{}> = null;

    /**
     * Constructor.
     */
    constructor(
        public navCtrl: NavController
    ) {
    }

    /**
     * Redirects the user to a requested player page.
     *
     * @param {Object} player
     */
    viewTopscorer(player) {
        this.navCtrl.push(PlayerPage, {
            playerId: player['id'],
            leagueId: this.league['id']
        });
    }
}
