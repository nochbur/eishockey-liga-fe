import { Component, Input } from '@angular/core';
import { PenaltiesHelper } from '../../utilities/penalties-helper';

@Component({
    selector: 'game-details',
    templateUrl: 'game-details.html'
})

export class GameDetailsComponent {
    @Input()
    gameActions: Array<{}> = null;

    /**
     * @param {PenaltiesHelper} penaltiesHelper
     */
    constructor(
        private penaltiesHelper: PenaltiesHelper
    ) {
    }

    /**
     * Returns the correct translation for the penalty.
     *
     * @param {string} key
     *
     * @return {string}
     */
    getPenaltyTranslation(key) {
        return this.penaltiesHelper.getPenaltyByKey(key);
    }
}