import { Component, Input } from '@angular/core';

@Component({
    selector: 'playoff-encounter-item',
    templateUrl: 'playoff-encounter-item.html'
})

export class PlayoffEncounterItemComponent {
    @Input()
    encounter: Object = null;

    homeTeam: Object = null;
    awayTeam: Object = null;

    /**
     * Triggered when input values change.
     */
    ngOnChanges() {
        this.buildEncounterData();
    }

    /**
     * Defines scope variables for home team and away team.
     */
    buildEncounterData() {
        this.homeTeam = {
            'id': this.encounter['teams'][0]['id'],
            'name': this.encounter['teams'][0]['longname'],
            'gamesWon': this.encounter['teams'][0]['gamesWon']
        };
        this.awayTeam = {
            'id': this.encounter['teams'][1]['id'],
            'name': this.encounter['teams'][1]['longname'],
            'gamesWon': this.encounter['teams'][1]['gamesWon']
        };
    }
}