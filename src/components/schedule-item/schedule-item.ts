import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GamePage } from '../../pages/game/game';
import { DateHelper } from '../../utilities/date-helper';

@Component({
    selector: 'schedule-item',
    templateUrl: 'schedule-item.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ScheduleItem {
    @Input()
    game: any = null;

    @Input()
    league: Object = null;

    @Input()
    showGameDate: boolean = false;

    isFutureGame: boolean = false;
    formattedGameDay: string;

    /**
     * @param {navCtrl} NavController
     * @param {dateHelper} DateHelper
     */
    constructor(
        public navCtrl: NavController,
        public dateHelper: DateHelper
    ) {
    }

    /**
     * Triggered when scope input changes.
     *
     * @param {Object} changes
     */
    ngOnChanges(changes) {
        this.prepareData();
        this.checkGameStatus();
    }

    /**
     * Prepares all needed data and scope variables.
     */
    prepareData() {
        this.formattedGameDay = this.dateHelper.getFormattedGameDay(this.game['scheduledDate']['value']);
    }

    /**
     * Redirects the user to a requested game page.
     */
    viewGame() {
        this.navCtrl.push(GamePage, {
            gameId: this.game['id'],
            gameData: this.game,
            league: this.league
        });
    }

    /**
     * Checks if the current game is still running or not.
     *
     * return {Boolean}
     */
    private checkGameStatus() {
        this.isFutureGame = this.dateHelper.checkIfGameIsFutureGame(
            this.game['scheduledDate']['value'],
            this.game['scheduledTime']
        );

        if (this.isFutureGame) {
            return;
        }

        // Make sure there is a score set.
        this.game['homeTeamScore'] = this.game['homeTeamScore'] || 0;
        this.game['awayTeamScore'] = this.game['awayTeamScore'] || 0;
    }
}