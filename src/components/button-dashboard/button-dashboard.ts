import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { Nav, NavController } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';

@Component({
    selector: 'button-dashboard',
    templateUrl: 'button-dashboard.html'
})

export class ButtonDashboardComponent {
    @ViewChild(Nav) nav: Nav;

    @Output()
    onPageChanged = new EventEmitter<Object>();

    /**
     * Constructor.
     */
    constructor(public navCtrl: NavController) {}

    /**
     * Redirects the user to the dashboard.
     */
    redirectToDashboard() {
        let page = {
            title: 'Dashboard',
            component: DashboardPage
        };
        this.onPageChanged.emit(page);
        this.navCtrl.setRoot(DashboardPage, {});
    }
}
