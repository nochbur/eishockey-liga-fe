import * as moment from 'moment';
import { Injectable } from '@angular/core';

@Injectable()
export class DateHelper {
    private ENGLISH_FULL_DATE_FORMAT = 'YYYY-MM-DDThh:mm:ss';
    private ENGLISH_DATE_FORMAT = 'YYYY-MM-DD';
    private WESTERN_FULL_DATE_FORMAT = 'DD.MM.YYYYThh:mm:ss';
    private WESTERN_DATE_FORMAT = 'DD.MM.YYYY';

    /**
     * Constructor
     */
    constructor () {
        moment.locale('de', {
            monthsShort : ['Jan', 'Feb', 'März', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
            monthsParseExact : true,
            weekdays : ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
            weekdaysParseExact : true,
        });
    }

    /**
     * Returns a unix timestamp by a given date string.
     *
     * @param {String} dateString
     *
     * @return {Number}
     */
    retrieveUnixTimestamp(dateString) {
        let time = 'T00:00:01';
        let formattedDate = moment(dateString, this.WESTERN_DATE_FORMAT).format(this.ENGLISH_DATE_FORMAT);
        let date = moment(formattedDate + time, this.ENGLISH_FULL_DATE_FORMAT);

        return date.unix();
    }

    /**
     * Returns a unix timestamp of the current date.
     *
     * @return {Number}
     */
    retrieveTodayUnixTimestamp() {
        let time = 'T00:00:01';
        let dateString = moment().format(this.ENGLISH_DATE_FORMAT);
        let date = moment(dateString + time, this.ENGLISH_FULL_DATE_FORMAT);

        return date.unix();
    }

    /**
     * Returns a moment object for a given date string.
     *
     * @param {String} dateString
     * @param {String} format
     *
     * @return {Object}
     */
    getMomentObj(dateString, format) {
        return moment(dateString, format);
    }

    /**
     * Returns a moment object for today.
     *
     * @return {Object}
     */
    getMoment() {
        return moment();
    }

    /**
     * Checks whether the game is in the future or should already be running.
     *
     * @param {String} scheduleDate 'DD.MM.YYYY'
     * @param {String} scheduleTime 'hh:mm'
     *
     * @return {Boolean}
     */
    checkIfGameIsFutureGame(scheduleDate, scheduleTime) {
        let now = this.getMoment();
        let gameDate = this.getMomentObj(
            scheduleDate + 'T' + scheduleTime + ':00',
            this.WESTERN_FULL_DATE_FORMAT
        );

        return now.isBefore(gameDate);
    }

    /**
     * Returns a formatted game date. eg. Sonntag, 01.02.2017
     *
     * @param {string} dateString
     *
     * @return {String}
     */
    getFormattedGameDay(dateString) {
        if (!dateString) {
            return '';
        }

        let date = this.getMomentObj(dateString, 'DD.MM.YYYY');

        if (!date.isValid()) {
            return '';
        }

        return date.format('dddd, DD.MM.YYYY');;
    }
}
