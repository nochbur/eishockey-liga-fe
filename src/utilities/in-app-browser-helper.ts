import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

export class InAppBrowserInterface {
    protected inAppBrowser: InAppBrowser;

    public inAppBrowserOptions: InAppBrowserOptions = {
        location: 'yes',
        hidden: 'no',
        clearcache: 'yes',
        clearsessioncache: 'yes',
        zoom: 'yes',
        hardwareback: 'yes',
        mediaPlaybackRequiresUserAction: 'no',
        shouldPauseOnSuspend: 'no',
        closebuttoncaption: 'Close',
        disallowoverscroll: 'no',
        toolbar: 'yes',
        enableViewportScale: 'no',
        allowInlineMediaPlayback: 'no',
        presentationstyle: 'pagesheet',
        fullscreen: 'yes',
    };

    /**
     * Constructor.
     */
    constructor(inAppBrowser: InAppBrowser) {
        this.inAppBrowser = inAppBrowser;
    }

    /**
     * Redirects to the detail page of an item.
     */
    public openUrlInAppBrowser(url) {
        let target = '_system';
        this.inAppBrowser.create(url, target, this.inAppBrowserOptions);
    }
}
