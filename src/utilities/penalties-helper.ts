import { Injectable } from '@angular/core';

@Injectable()
export class PenaltiesHelper {
    private PENALTY_FALLBACK = 'Strafe';
    private PENALTIES_MAPPING = {
        'abuse': 'Beschimpfung von Offiziellen',
        'critic': 'Kritik am Entscheid',
        'bench': 'Kleine Bankstrafe',
        'trip': 'Beinstellen',
        'hi-st': 'Hoher Stock',
        'intrf': 'Behinderung',
        'hook': 'Haken',
        'hold': 'Halten',
        'slash': 'Stockschlag',
        'elbow': 'Ellbogencheck',
        'clip': 'Check gegen das Knie',
        'knee': 'Check mit dem Knie',
        'charg': 'Unerlaubter Körperangriff',
        'too-m': 'Zu viele Spieler auf dem Eis',
        'un-sp': 'Unsportliches Verhalten',
        'dive': 'Schwalbe',
        'board': 'Check gegen die Bande',
        'ho-st': 'Halten des Stockes',
        'delay': 'Spielverzögerung',
        'rough': 'Übertriebene Härte',
        'cross': 'Cross-Checking',
        'che-h': 'Check gegen den Kopf',
        'che-b': 'Check von hinten',
        'spear': 'Stockstich',
        'fight': 'Faustkampf',
        'pen-s': 'Penalty-Schuss',
        'misc': 'Disziplinarstrafe',
        'match': 'Matchstrafe',
        'ga-mi': 'Spieldauer-Disziplinarstrafe',
    };

    /**
     * Returns a translation according to the given key.
     *
     * @param {string} key
     *
     * @return {string}
     */
    getPenaltyByKey(key) {
        let normalizedKey = key.toLowerCase();

        if (!this.PENALTIES_MAPPING[normalizedKey]) {
            return this.PENALTY_FALLBACK;
        }

        return this.PENALTIES_MAPPING[normalizedKey];
    }
}