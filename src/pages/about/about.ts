import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { InAppBrowserInterface } from '../../utilities/in-app-browser-helper';

@IonicPage()
@Component({
    selector: 'about-page',
    templateUrl: 'about.html'
})

export class AboutPage extends InAppBrowserInterface {

    constructor(
        inAppBrowser: InAppBrowser
    ) {
        super(inAppBrowser);
    }

    /**
     * Redirects to a link.
     */
    openLinkInAppBrowser(link) {
        this.openUrlInAppBrowser(link);
    }
}
