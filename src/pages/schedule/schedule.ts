import { delay, find, last } from 'lodash';
import * as moment from 'moment';
import { Component, ViewChild } from '@angular/core';
import { NavParams, FabContainer } from 'ionic-angular';
import { ScheduleService } from '../../services/api';
import { BasePage } from '../../app/base.page';

@Component({
    selector: 'schedule-page',
    templateUrl: 'schedule.html'
})

export class SchedulePage extends BasePage {
    @ViewChild(FabContainer) fab: FabContainer;

    leagueConfig: Object = {};
    scheduleData: Object = {};

    preliminaryGames: Array<{}> = [];
    intermediateGames: Array<{}> = [];
    viewableGames: Array<{}> = [];

    activeMonth: any = '';
    filterMonth: Array<{}> = [];

    /**
     * @param {NavParams} navParams
     * @param {ScheduleService} scheduleService
     */
    constructor(
        public navParams: NavParams,
        private scheduleService: ScheduleService
    ) {
        super();
        this.leagueConfig = this.navParams.get('league');
        this.fetchFromApi();
    }

    /**
     * Fetch all data which are needed for this page.
     */
    fetchFromApi() {
        this.fetchScheduleList();
    }

    /**
     * Triggered when switching the submenu.
     *
     * @param {String} page
     */
    switchSubMenu(page: string) {
        this.submenu = page;
        this.viewableGames = this.retrieveViewableGames();
        this.filterMonth = this.retrieveFilterMonth();
        this.scrollToLatestDate();
        this.fab.close();
    }

    /**
     * Triggered when month is changed.
     *
     * @param {Moment} month
     */
    changeMonth(month) {
        this.activeMonth = month.month;
        this.viewableGames = this.retrieveViewableGames();
        this.fab.close();
        this.content.scrollToTop();
    }

    /**
     * Fetches the schedule list.
     */
    private fetchScheduleList() {
        this.showSpinner = true;
        this.scheduleService.retrieveGroupedGames(this.leagueConfig['id'])
            .subscribe(
                data => this.onScheduleListFetched(data, null),
                error => this.onScheduleListFetched(null, error)
            );
    }

    /**
     * Triggered when the schedule list is fetched.
     *
     * @param {Object|null} data
     * @param {Object|null} error
     */
    private onScheduleListFetched(data, error) {
        if (error) {
            this.onApiError(error);
            return false;
        }

        this.scheduleData = data;

        if (this.scheduleData['intermediate']) {
            this.intermediateGames = this.groupGameDaysByMonth(this.scheduleData['intermediate']);
            this.submenu = 'intermediate';
        }

        if (this.scheduleData['preliminary']) {
            this.preliminaryGames = this.groupGameDaysByMonth(this.scheduleData['preliminary']);
            this.submenu = 'preliminary';
        }

        this.activeMonth = this.retrieveActiveMonth();
        this.viewableGames = this.retrieveViewableGames();
        this.filterMonth = this.retrieveFilterMonth();
        this.showSpinner = false;
        this.scrollToLatestDate();
    }

    /**
     * TODO: Fix this.
     * Scrolls to the latest game.
     */
    private scrollToLatestDate() {
        let timeString = this.scheduleData['today'];
        let gameDay = find(this.viewableGames, function(item) {
            return item.date.sortValue == timeString || item.date.sortValue > timeString;
        });

        if (!gameDay) {
            this.content.scrollToTop();
        } else {
            this.scrollToAnchor('divider-' + gameDay.date.sortValue);
        }
    }

    /**
     * TODO: add break for delay loop
     * Scrolls the ui to a given anchor point.
     *
     * @param {String} anchor
     * @param {Number} loop
     */
    private scrollToAnchor(anchor: string, loop?: number) {
        let elem = document.getElementById(anchor);

        if (!loop) {
            loop = 1;
        }

        if (!elem) {
            loop += 1;
            delay(function() {
                this.scrollToAnchor(anchor, loop);
            }.bind(this), 100);

            return;
        }

        let yOffset = elem.offsetTop;

        this.content.scrollTo(0, yOffset, 100);
    }

    /**
     * Returns the current phase games.
     *
     * @return {Array}
     */
    private retrieveCurrentPhase() {
        let phase = this.preliminaryGames;

        if (this.submenu === 'intermediate') {
            phase = this.intermediateGames;
        }

        return phase;
    }

    /**
     * Returns the games of the current month.
     *
     * @return {Array}
     */
    private retrieveActiveMonth() {
        let offseason = [5, 6, 7];
        let currentMonth = this.retrieveCurrentMonth();
        let phase = this.retrieveCurrentPhase();

        if (find(phase, { 'month': currentMonth })) {
            return currentMonth;
        }

        if (offseason.indexOf(currentMonth)) {
            return last(phase)['month'];
        }

        return phase[0]['month'];
    }

    /**
     * Returns the possible month for the filter.
     *
     * @return {Array}
     */
    private retrieveFilterMonth() {
        let phase = this.retrieveCurrentPhase();
        let result = [];

        for (let key in phase) {
            result.push({
                'title': phase[key]['title'],
                'month': phase[key]['month']
            })
        }

        return result;
    }

    /**
     * Returns a list of games which are viewable.
     *
     * @return {Array}
     */
    private retrieveViewableGames() {
        let phase = this.retrieveCurrentPhase();

        if (find(phase, { 'month': this.activeMonth })) {
            return find(phase, { 'month': this.activeMonth })['gameDays'];
        }

        return phase[0]['gameDays'];
    }

    /**
     * Returns the current month.
     *
     * @return {String}
     */
    private retrieveCurrentMonth() {
        return moment().month();
    }

    /**
     * Returns the month by a given date.
     *
     * @param {Date} date
     *
     * @return {String}
     */
    private retrieveMonthByDate(date) {
        return moment(date, 'DD.MM.YYYY').month();
    }

    /**
     * Returns the month name by a given date.
     *
     * @param {Date} date
     *
     * @return {String}
     */
    private retrieveMonthStringByDate(date) {
        return moment(date, 'DD.MM.YYYY').format("MMM");
    }

    /**
     * Groups all days by their month and returns a grouped list.
     *
     * @param {Array} gameDays
     *
     * @return {Array}
     */
    private groupGameDaysByMonth(gameDays) {
        let result = [];

        for (let gameDay of gameDays) {
            let month = this.retrieveMonthByDate(gameDay['date']['value']);
            let item = find(result, { 'month': month });

            if (item) {
                item['gameDays'].push(gameDay);
            } else {
                result.push({
                    'title': this.retrieveMonthStringByDate(gameDay['date']['value']),
                    'month': month,
                    'gameDays': [gameDay]
                });
            }
        }

        return result;
    }
}
