import { Component } from '@angular/core';
import { BasePage } from '../../app/base.page';
import { NewsService } from '../../services/api';

@Component({
    selector: 'news-room-page',
    templateUrl: 'news-room.html'
})
export class NewsRoomPage extends BasePage {

    submenu: string = 'one';

    newsAt: Array<{}> = [];
    newsCh: Array<{}> = [];

    /**
     * Constructor.
     */
    constructor(
        private newsService: NewsService
    ) {
        super();
        this.fetchFromApi();
    }

    /**
     * Fetches the latest news.
     */
    fetchFromApi() {
        this.showSpinner = true;
        this.newsService.retrieveNews()
            .subscribe(
                data => this.onNewsFetched(data, null),
                error => this.onNewsFetched(null, error)
            );
    }

    /**
     * Triggered when the news are fetched.
     *
     * @param {Object|null} data
     * @param {Object|null} error
     */
    onNewsFetched(data, error) {
        if (error) {
            this.onApiError(error);
            return false;
        }

        if (data['news']) {
            this.newsAt = data['news']['AT'] || [];
            this.newsCh = data['news']['CH'] || [];
        }

        this.submenu = 'at';

        this.showSpinner = false;
    }
}
