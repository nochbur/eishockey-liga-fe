import { reverse, find, last } from 'lodash';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { KnockoutService } from '../../services/api';
import { BasePage } from '../../app/base.page';

@Component({
    selector: 'playoff-page',
    templateUrl: 'playoff.html'
})

export class PlayoffPage extends BasePage {

    leagueConfig: Object = {};
    playoffData: Array<{}> = [];
    playoffMapping: any = [
        {
            index: 0,
            title: 'Finale',
            key: 'finale'
        }, {
            index: 1,
            title: '1/2 Finale',
            key: 'half-finale'
        }, {
            index: 2,
            title: '1/4 Finale',
            key: 'quater-finale'
        }, {
            index: 3,
            title: '1/8 Finale',
            key: 'eighth-finale'
        }, {
            index: 4,
            title: '1/16 Finale',
            key: 'Sixteenth-finale'
        }
    ];

    /**
     * Constructor.
     */
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private knockoutService: KnockoutService
    ) {
        super();
        this.leagueConfig = this.navParams.get('league');
        this.fetchFromApi();
    }

    /**
     * Fetches all data which are needed by the page.
     */
    fetchFromApi() {
        this.fetchPlayoffData();
    }

    /**
     * Fetch playoff data.
     */
    private fetchPlayoffData() {
        this.showSpinner = true;
        this.knockoutService.retrieveKnockout(this.leagueConfig['id'])
            .subscribe(
                data => this.onDataFetched(data, null),
                error => this.onDataFetched(null, error)
            );
    }

    /**
     * Triggered when playoff data are fetched.
     *
     * @param {Object|null} data
     * @param {Object|null} error
     */
    private onDataFetched(data, error) {
        if (error) {
            this.onApiError(error);
            return false;
        }

        if (data && data['playoff']) {
            this.playoffData = data['playoff'],
            this.enrichPlayoffData();
        }

        this.showSpinner = false;
    }

    /**
     * Enrich the playoff data for the front end.
     */
    private enrichPlayoffData() {
        this.playoffData = reverse(this.playoffData);

        // Add title and key to each playoff phase.
        for (let i = 0; i < this.playoffData.length; i++) {
            this.playoffData[i]['customTitle'] = '';
            this.playoffData[i]['customKey'] = '';

            if (this.playoffMapping[i]) {
                this.playoffData[i]['customTitle'] = this.playoffMapping[i].title;
                this.playoffData[i]['customKey'] = this.playoffMapping[i].key;
            }
        }

        let activeRound = find(this.playoffData, function(item) {
            return item['hasStarted'] && item['hasStarted'] === true;
        });

        if (activeRound) {
            this.submenu = activeRound['customKey'];
        } else {
            this.submenu = last(this.playoffData)['customKey'];
        }

        this.playoffData = reverse(this.playoffData);
    }
}
