import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LeagueConfigService } from '../../services/api';
import { ScheduleService } from '../../services/api';
import { BasePage } from '../../app/base.page';
import { SchedulePage } from '../schedule/schedule';
import { PlayoffPage } from '../playoff/playoff';

@Component({
    selector: 'dashboard-page',
    templateUrl: 'dashboard.html'
})

export class DashboardPage extends BasePage {
    private GAME_DAY_LIMIT = 1;

    dashboardMainLeague: Object = {};
    dashboardMainLeagueId: string = '';

    lastGames: Array<{}> = [];
    todayGames: Array<{}> = [];
    nextGames: Array<{}> = [];

    showSpinnerTodayGames: boolean = false;
    showSpinnerNextGames: boolean = false;
    showSpinnerLatestGames: boolean = false;

    /**
     * @param {ScheduleService} scheduleService
     * @param {LeagueConfigService} leagueConfigService
     * @param {NavController} navCtrl
     */
    constructor(
        private scheduleService: ScheduleService,
        private leagueConfigService: LeagueConfigService,
        public navCtrl: NavController
    ) {
        super();

        // Get the latest league config. This ensures, that the config is updated now and then.
        this.leagueConfigService.getSettings();

        // Listen to changes of the stored league settings.
        this.leagueConfigService.event.subscribe((leagues) => {
            this.onLeagueSettingsFetched(leagues)
        });
    }

    /**
     * Refreshes the dashboard.
     */
    doRefresh(refresher) {
        this.fetchDashboardData();
        refresher.complete();
    }

    /**
     * Redirects to the playoff page of the main league.
     * If playoff has not started yet it redirects to the schedule page.
     */
    openSchedulePage() {
        if (this.dashboardMainLeague['playoff']) {
            this.navCtrl.setRoot(
                PlayoffPage,
                {
                    'league': this.dashboardMainLeague
                },
                {
                    animate: true
                }
            );
        } else {
            this.navCtrl.setRoot(
                SchedulePage,
                {
                    'league': this.dashboardMainLeague
                },
                {
                    animate: true
                }
            );
        }
    }

    /**
     * Triggered when league settings got fetched.
     *
     * @param {Object|null} data
     */
    private onLeagueSettingsFetched(data) {
        // As the dashboard will always show the hightest league, we select it hard here.
        this.dashboardMainLeagueId = '';
        this.dashboardMainLeague = {};

        if (data && data.length) {
            this.dashboardMainLeagueId = data[0]['leagues'][0]['id'];
            this.dashboardMainLeague = data[0]['leagues'][0];
        }

        this.fetchDashboardData();
    }

    /**
     * Fetches the dashboard data.
     */
    private fetchDashboardData() {
        this.fetchTodayGames();
        this.fetchLatestGames();
        this.fetchNextGames();
    }

    /**
     * Fetch todays games for the main league.
     */
    private fetchTodayGames() {
        this.showSpinnerTodayGames = true;
        this.scheduleService.retrieveTodayGames(this.dashboardMainLeagueId)
            .subscribe(
                data => this.onTodayGamesFetched(data, null),
                error => this.onTodayGamesFetched(null, error)
            );
    }

    /**
     * Fetch the latest games for the main league.
     */
    private fetchLatestGames() {
        this.showSpinnerLatestGames = true;
        this.scheduleService.retrieveLatestGames(this.dashboardMainLeagueId)
            .subscribe(
                data => this.onLatestGamesFetched(data, null),
                error => this.onLatestGamesFetched(null, error)
            );
    }

    /**
     * Fetch the next games for the main league.
     */
    private fetchNextGames() {
        this.showSpinnerNextGames = true;
        this.scheduleService.retrieveNextGames(this.dashboardMainLeagueId)
            .subscribe(
                data => this.onNextGamesFetched(data, null),
                error => this.onNextGamesFetched(null, error)
            );
    }

    /**
     * Triggered when todays games got fetched.
     *
     * @param {Object|null} data
     * @param {Object|null} error
     */
    private onTodayGamesFetched(data, error) {
        if (error) {
            this.onApiError(error);
            this.showSpinnerTodayGames = false;

            return false;
        }

        this.todayGames = this.getProperty(data, 'games');
        this.showSpinnerTodayGames = false;
    }

    /**
     * Triggered when latest games got fetched.
     *
     * @param {Object|null} data
     * @param {Object|null} error
     */
    private onLatestGamesFetched(data, error) {
        if (error) {
            this.onApiError(error);
            this.showSpinnerLatestGames = false;

            return false;
        }

        this.lastGames = this.getProperty(data, 'games');

        if (this.lastGames) {
            this.lastGames = this.lastGames.splice(0, this.GAME_DAY_LIMIT);
        }
        this.showSpinnerLatestGames = false;
    }

    /**
     * Triggered when next games got fetched.
     *
     * @param {Object|null} data
     * @param {Object|null} error
     */
    private onNextGamesFetched(data, error) {
        if (error) {
            this.onApiError(error);
            this.showSpinnerNextGames = false;

            return false;
        }

        this.nextGames = this.getProperty(data, 'games');

        if (this.nextGames) {
            this.nextGames = this.nextGames.splice(0, this.GAME_DAY_LIMIT);
        }
        this.showSpinnerNextGames = false;
    }
}
