import { Component } from '@angular/core';
import { BasePage } from '../../app/base.page';
import { YoutubeService } from '../../services/api';

@Component({
    selector: 'video-room-page',
    templateUrl: 'video-room.html'
})
export class VideoRoomPage extends BasePage {

    youtubeBaseUrl: string = 'https://www.youtube.com/embed/';
    playlists: Array<{}> = [];
    youtubeData: Object = {};
    videos: Array<{}> = [];
    currentPlaylistId: string = '';

    /**
     * Constructor.
     */
    constructor(
        private youtubeService: YoutubeService
    ) {
        super();

        this.playlists = [
            {
                'title': 'Österreich',
                'key': 'highlights',
                'playlistId': 'PLDgz5N9PNwuv4BQso444uEAYm-B4XCkdj'
            },
            {
                'title': 'Welt',
                'key': 'international',
                'playlistId': 'PLDgz5N9PNwuuQdnAFhZIGB9ZMTBiOFALK'
            }
        ];

        this.currentPlaylistId = this.playlists[0]['playlistId'];
        this.fetchVideos(null);
        this.submenu = 'highlights';
    }

    /**
     * Triggerd to remove the preview image.
     *
     * @param {Object} video
     */
    showVideo(video) {
        video['loadVideo'] = true;
    }

    /**
     * Returns the thumbnail url for a video.
     *
     * @param {Object} video
     *
     * @return {String}
     */
    getThumbnailUrl(video) {
        let thumbs = video['snippet']['thumbnails'];

        if (!thumbs) {
            return '';
        }

        if (thumbs['maxres']) {
            return thumbs['maxres']['url'];
        } else if (thumbs['high']) {
            return thumbs['high']['url'];
        } else if (thumbs['medium']) {
            return thumbs['medium']['url'];
        } else if (thumbs['standard']) {
            return thumbs['standard']['url'];
        } else if (thumbs['default']) {
            return thumbs['default']['url'];
        }

        return '';
    }

    /**
     * Triggered when a submenu item is clicked.
     */
    switchSubMenu(playlist) {
        this.content.scrollToTop();
        this.submenu = playlist['key'];
        this.currentPlaylistId = playlist['playlistId'];
        this.fetchVideos(null);
    }

    /**
     * Fetches videos from youtube.
     *
     * @param {String} pageToken
     */
    private fetchVideos(pageToken) {
        this.showSpinner = true;
        this.videos = [];

        let params = {
            'playlistId': this.currentPlaylistId,
            'pageToken': pageToken
        };

        this.youtubeService.fetchPlaylist(params).subscribe(
            data => this.onVideosFetched(data, null),
            error => this.onVideosFetched(null, error)
        );
    }

    /**
     * Triggered when the youtube videos are fetched.
     *
     * @param {Object|null} data
     * @param {Object|null} error
     */
    private onVideosFetched(data, error) {
        if (error) {
            this.onApiError(error);
            return false;
        }

        this.showSpinner = false;
        this.youtubeData = data || {},
        this.videos = this.youtubeData['items'] || [];
    }
}
