import { cloneDeep } from 'lodash';
import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { GameReportService } from '../../services/api';
import { DateHelper } from '../../utilities/date-helper';
import { BasePage } from '../../app/base.page';

@Component({
    selector: 'game-page',
    templateUrl: 'game.html'
})

export class GamePage extends BasePage {
    leagueConfig: Object = {};
    gameDataCached: any = {};

    isFutureGame: boolean = true;
    isGameRunning: boolean = false;
    isGameDataDelayed: boolean = false;
    gameOverview: Object = {};
    gameOfficials: Object = {};
    gameActions: Array<{}> = [];
    gameLineup: Object = {};
    gameStatistics: Object = {};

    encounterDetails: Object = {};

    /**
     * @param {NavParams} navParams
     * @param {GameReportService} gameReportService
     * @param {DateHelper} dateHelper
     */
    constructor(
        private navParams: NavParams,
        private gameReportService: GameReportService,
        private dateHelper: DateHelper
    ) {
        super();
        this.submenu = 'details';
        this.leagueConfig = this.navParams.get('league');
        this.gameDataCached = this.navParams.get('gameData');
        this.buildEncounterDetails();
        this.fetchFromApi();
    }

    /**
     * TODO: set timer to fetch new data.
     * Fetches all needed data for the page.
     */
    fetchFromApi() {
        if (!this.isFutureGame) {
            this.fetchGameReport();
        }
    }

    /**
     * Fetches the current game report.
     */
    private fetchGameReport() {
        this.showSpinner = true;
        let gameId = this.navParams.get('gameId');

        this.gameReportService.retrieveGameReport(this.leagueConfig['id'], gameId)
            .subscribe(
                data => this.onGameReportFetched(data, null),
                error => this.onGameReportFetched(null, error)
            );
    }

    /**
     * Triggered when game data are fetched.
     *
     * @param {Object|null} data
     * @param {Object|null} error
     *
     * @return {Boolean}
     */
    private onGameReportFetched(data, error) {
        if (error) {
            this.onApiError(error);

            return false;
        }

        if (error === null && data === null) {
            let errorMessage = 'Error while fetching game report.';

            this.onApiError(errorMessage);

            return false;
        }

        if (data === null) {
            this.checkIfIsFutureGameday();
            this.isGameDataDelayed = !this.isFutureGame;
            this.showSpinner = false;

            return true;
        }

        this.gameOverview = data.overview || null;
        this.gameOfficials = data.officials || null;
        this.gameActions = data.actions || null;
        this.gameLineup = data.lineup || null;
        this.gameStatistics = data.statistics || null;
        this.updateEncounterDetails();
        this.isFutureGame = false;
        this.showSpinner = false;

        return true;
    }

    /**
     * Sets the correct dat
     */
    private checkIfIsFutureGameday() {
        this.isFutureGame = this.dateHelper.checkIfGameIsFutureGame(
            this.encounterDetails['scheduledDate']['value'],
            this.encounterDetails['scheduledTime']
        );
    }

    /**
     * Restructre data to build encounter detail view.
     */
    private buildEncounterDetails() {
        let periodScore = {
            homeScore: 0,
            awayScore: 0
        };
        let defaultPeriodStats = [periodScore, periodScore, periodScore];

        this.encounterDetails = cloneDeep(this.gameDataCached);

        this.encounterDetails['homeTeam'] = {
            'id': this.gameDataCached.homeTeamId,
            'name': this.gameDataCached.homeTeamLongName
        };

        this.encounterDetails['awayTeam'] = {
            'id': this.gameDataCached.awayTeamId,
            'name': this.gameDataCached.awayTeamLongName
        };

        this.encounterDetails['periodStats'] = defaultPeriodStats;
        this.encounterDetails['gameStatus'] = 0;
        this.checkIfIsFutureGameday();
    }

    /**
     * Update existing data set.
     */
    private updateEncounterDetails() {
        this.encounterDetails['gameStatus'] = this.gameOverview['gameStatus'];
        this.encounterDetails['periodStats'] = this.gameOverview['periodStats'];
        this.encounterDetails['homeTeamScore'] = this.gameOverview['homeTeamScore'];
        this.encounterDetails['awayTeamScore'] = this.gameOverview['awayTeamScore'];

        if (this.encounterDetails['gameStatus'] > 0 && this.encounterDetails['periodStats']) {
            this.isGameRunning = true;
        } else {
            this.isGameRunning = false;
        }
    }
}
