import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { StandingsService } from '../../services/api';
import { BasePage } from '../../app/base.page';

@Component({
    selector: 'standings-page',
    templateUrl: 'standings.html'
})

export class StandingsPage extends BasePage {

    leagueConfig: Object = {};
    standingsData: Object = {};
    preliminaryStandings: Array<{}> = [];
    intermediateStandings: Array<{}> = [];

    /**
     * Constructor.
     */
    constructor(
        public navParams: NavParams,
        private standingsService: StandingsService
    ) {
        super();
        this.leagueConfig = this.navParams.get('league');
        this.fetchFromApi();
    }

    /**
     * Fetches all needed data for the view.
     */
    fetchFromApi() {
        this.fetchStandings();
    }

    /**
     * Fetches the current standings.
     */
    private fetchStandings() {
        this.showSpinner = true;
        this.standingsService.retrieveStandings(this.leagueConfig['id'])
            .subscribe(
                data => this.onStandignsFetched(data, null),
                error => this.onStandignsFetched(null, error)
            );
    }

    /**
     * Triggered when the standings are fetched.
     *
     * @param {Object|null} data
     * @param {Object|null} error
     */
    private onStandignsFetched(data, error) {
        if (error) {
            this.onApiError(error);
            return false;
        }

        this.preliminaryStandings = this.getProperty(data, 'preliminary');
        this.intermediateStandings = this.getProperty(data, 'intermediate');
        this.submenu = 'preliminary';
        this.showSpinner = false;
    }
}
