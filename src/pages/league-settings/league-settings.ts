import { Component } from '@angular/core';
import { BasePage } from '../../app/base.page';
import { LeagueConfigService } from '../../services/api';

@Component({
    selector: 'league-settings',
    templateUrl: 'league-settings.html'
})

export class LeagueSettingsPage extends BasePage {
    leagueGroups: Array<{}> = [];

    /**
     * @param {LeagueConfigService} leagueConfigService
     */
    constructor(
        private leagueConfigService: LeagueConfigService
    ) {
        super();

        // Always fetch the latest settings from the api.
        this.fetchLeagueSettings();

        // Listen to changes of the stored league settings.
        this.leagueConfigService.event.subscribe((leagues) => {
            this.onStoredLeaguesFetched(leagues)
        });
    }

    /**
     * Triggered from the ui when a league is selected or deselected.
     */
    onSettingsChanged() {
        this.leagueConfigService.setStoredLeagues(this.leagueGroups, false);
    }

    /**
     * Triggers the league api fetch.
     */
    private fetchLeagueSettings() {
        this.showSpinner = true;
        this.leagueConfigService.getSettings();
    }

    /**
     * Is triggered when the league api fetch is completed.
     *
     * @param {Array} leagues
     */
    private onStoredLeaguesFetched(leagues) {
        this.showSpinner = false;
        this.leagueGroups = leagues;
    }
}
