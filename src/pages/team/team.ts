import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { TeamService } from '../../services/api';
import { BasePage } from '../../app/base.page';

@Component({
    selector: 'team-page',
    templateUrl: 'team.html'
})

export class TeamPage extends BasePage {
    leagueConfig = null;
    teamId = null;
    teamName = '';
    team = null;

    /**
     * @param {NavParams} navParams
     * @param {TeamService} teamService
     */
    constructor(
        private navParams: NavParams,
        private teamService: TeamService,
    ) {
        super();
        this.submenu = 'details';
        this.leagueConfig = this.navParams.get('leagueConfig');
        this.teamId = this.navParams.get('teamId');
        this.fetchTeamDetails();
    }

    /**
     * Fetches the teams api.
     */
    private fetchTeamDetails() {
        this.showSpinner = true;
        this.teamService.retrieveTeamDetails(this.leagueConfig.id, this.teamId)
            .subscribe(
                data => this.onTeamDetailsFetched(data, null),
                error => this.onTeamDetailsFetched(null, error)
            );
    }

    /**
     * Triggered when team data are fetched.
     *
     * @param {Object|null} data
     * @param {Object|null} error
     *
     * @return {Boolean}
     */
    private onTeamDetailsFetched(data, error) {
        if (error || !data.team) {
            this.onApiError(error);

            return false;
        }

        this.showSpinner = false;
        this.team = data.team;
        this.teamName = this.team.information.teamLongname;

        if (this.team.games.next.length > 3) {
            this.team.games.next = this.team.games.next.splice(0, 3);
        }

        if (this.team.games.last.length > 3) {
            this.team.games.last = this.team.games.last.splice(-3);
        }
    }
}
