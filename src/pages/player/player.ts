import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { BasePage } from '../../app/base.page';
import { PlayerService } from '../../services/api';

@Component({
    selector: 'player-page',
    templateUrl: 'player.html'
})

export class PlayerPage extends BasePage {
    private emptyFallback = '-';
    private positionMapping = {
        'F': 'Stürmer',
        'D': 'Verteidiger',
        'G': 'Tormann'
    };

    playerData: Object = {};
    overviewLines: Array<{}> = [];
    overviewColumnNames: Array<String> = ['', ''];
    overviewColumnWidth: Array<Number> = [7, 5];
    statsLines: Array<{}> = [];
    statsColumnNames: Array<String> = ['', ''];
    statsColumnWidth: Array<Number> = [7, 5];

    constructor(
        public navParams: NavParams,
        private playerService: PlayerService
    ) {
        super();
        this.showSpinner = true;
        this.submenu = 'overview';
        this.fetchPlayerDetails();
    }

    /**
     * Fetch details about the player over an API.
     */
    private fetchPlayerDetails() {
        let leagueId = this.navParams.get('leagueId');
        let playerId = this.navParams.get('playerId');

        this.playerService.retrievePlayerData(leagueId, playerId)
            .subscribe(
                data => this.onPlayerDetailsFetched(data, null),
                error => this.onPlayerDetailsFetched(null, error)
            );
    }

    /**
     * Triggered when player details are fetched.
     *
     * @param {Object|null} data
     * @param {Object|null} error
     */
    private onPlayerDetailsFetched(data, error) {
        if (error) {
            this.onApiError(error);
            return false;
        }

        if (error === null && data === null) {
            let errorMessage = 'Error while fetching player details.';
            this.onApiError(errorMessage);

            return false;
        }

        this.playerData = data.player;
        this.prepareOverview();

        if (this.playerData['playerData']['position'] == 'G') {
            this.prepareKeeperStats();
        } else {
            this.preparePlayerStats();
        }
    }

    /**
     * Prepares a dataset from the playerdata for the overview table.
     */
    private prepareOverview() {
        // Convenience variable.
        let player = this.playerData['playerData'];

        // Calculating the age from the diffToNow data we got from the API.
        let age = this.calculateAge();

        let position = player['position'] || this.emptyFallback;
        let playerNumber = player['number'] || this.emptyFallback;
        let teamLongname = player['teamLongname'] || this.emptyFallback;
        let height = player['height'] ? player['height'] + ' cm' : this.emptyFallback;
        let weight = player['weight'] ? player['weight'] + ' kg' : this.emptyFallback;
        let nation = player['nation'] || this.emptyFallback;
        let playerBirthdate = player['playerBirthdate']['formattedShort'] || this.emptyFallback;

        // Set the data for the lines of the overview table.
        this.overviewLines = [
            ['Position', this.positionMapping[position] || this.emptyFallback],
            ['Nummer', playerNumber],
            ['Team', teamLongname],
            ['Größe', height],
            ['Gewicht', weight],
            ['Nationalität', nation],
            ['Geburtstag', playerBirthdate],
            ['Alter', age],
        ];

        this.showSpinner = false;
    }

    /**
     * Prepares a dataset from the playerdata for the overview table.
     */
    private preparePlayerStats() {
        // Convenience variable.
        let stats = this.playerData['playerStats'];

        let gamesPlayed = stats['gamesPlayed'] || this.emptyFallback;
        let assists = stats['assists'] || this.emptyFallback;
        let goals = stats['goals'] || this.emptyFallback;
        let points = stats['points'] || this.emptyFallback;
        let minorPenalties = stats['minorPenalties'] || this.emptyFallback;
        let matchPenalties = stats['matchPenalties'] || this.emptyFallback;
        let gameMisconducts = stats['gameMisconducts'] || this.emptyFallback;
        let penaltyMinutes = stats['penaltyMinutes'] || this.emptyFallback;
        let penaltyMinutesPerGame = stats['penaltyMinutesPerGame'] || this.emptyFallback;


        // Set the data for the lines of the overview table.
        this.statsLines = [
            ['Spiele gespielt', gamesPlayed],
            ['Assists', assists],
            ['Tore', goals],
            ['Punkte', points],
            ['Kleine Strafen', minorPenalties],
            ['Match Strafen', matchPenalties],
            ['Spielausschlüsse', gameMisconducts],
            ['Strafminuten', penaltyMinutes],
            ['Strafminuten pro Spiel', penaltyMinutesPerGame],
        ];

        this.showSpinner = false;
    }

    /**
     * Prepares a dataset from the playerdata for the overview table.
     */
    private prepareKeeperStats() {
        // Convenience variable.
        let stats = this.playerData['playerStats'];

        let gamesPlayed = stats['gamesPlayed'] || this.emptyFallback;
        let goalsAgainst = stats['goalsAgainst'] || this.emptyFallback;
        let shotsAgainst = stats['shotsAgainst'] || this.emptyFallback;
        let goalsAgainstAverage = stats['goalsAgainstAverage'] || this.emptyFallback;
        let savePercentage = stats['savePercentage'] || this.emptyFallback;
        let penaltyMinutes = stats['penaltyMinutes'] || this.emptyFallback;

        // Set the data for the lines of the overview table.
        this.statsLines = [
            ['Spiele gespielt', gamesPlayed],
            ['Tore bekommen', goalsAgainst],
            ['Tore pro Spiel', goalsAgainstAverage],
            ['Schüsse bekommen', shotsAgainst],
            ['Schussquote', savePercentage],
            ['Strafminuten', penaltyMinutes]
        ];

        this.showSpinner = false;
    }

    /**
    * Calculates the age of a player by the diffToNow variable.
    */
    private calculateAge() {
         let age = Math.abs(this.playerData['playerData']['playerBirthdate']['diffToNow'])
         // Convert seconds to hours.
         /3600
         // Convert hours to days.
         /24
         // Convert days to years.
         /365;

         // Round the float age to an int and return it.
         return Math.floor(age);
    }
}
