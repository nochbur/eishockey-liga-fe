import { ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';

export class BasePage {
    @ViewChild(Content) content: Content;

    /**
     * Define scope defaults.
     */
    submenu: string = '';
    showSpinner: boolean = false;

    /**
     * Constructor.
     */
    constructor() {}

    /**
     * Triggers the submenu switch.
     *
     * @param {String} page
     */
    switchSubMenu(page: string) {
        this.content.scrollToTop();
        this.submenu = page;
    }

    /**
     * Helper function to get a property of a given object.
     *
     * @param {Object} object
     * @param {String} propertyName
     * @param {any} fallback
     *
     * @return any
     */
    getProperty(object: Object, propertyName: string, fallback?: any) {
        if (object && object.hasOwnProperty(propertyName)) {
            return object[propertyName];
        }

        if (typeof fallback !== 'undefined') {
            return fallback;
        }

        return null;
    }

    /**
     * Triggered when user refreshes the page.
     *
     * @param {Object} refresher
     */
    doRefresh(refresher) {
        this.fetchFromApi();
        refresher.complete();
    }

    /**
     * Triggers the api fetch. Should be provided by the child controller.
     */
    fetchFromApi() {}

    /**
     * Is called if the api returns an error.
     *
     * @param {String} error
     */
    onApiError(error) {
        console.error('An error occured: ', error);
        this.showSpinner = false;
    }
}
