import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { IonicStorageModule } from '@ionic/storage';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ChartModule } from 'angular2-chartjs';

import { EishockeyLigaApp } from './app.component';
import { DateHelper } from '../utilities/date-helper';
import { PenaltiesHelper } from '../utilities/penalties-helper';

import { StandingsService } from '../services/api';
import { ScheduleService } from '../services/api';
import { GameReportService } from '../services/api';
import { KnockoutService } from '../services/api';
import { LeagueConfigService } from '../services/api';
import { HockeyDataConfigService } from '../services/api';
import { YoutubeService } from '../services/api';
import { NewsService } from '../services/api';
import { PlayerService } from '../services/api';
import { TeamService } from '../services/api';

import { ScheduleList } from '../components/schedule-list/schedule-list';
import { ScheduleItem } from '../components/schedule-item/schedule-item';
import { ScheduleLiveListComponent } from '../components/schedule-live-list/schedule-live-list';
import { ScheduleLiveItemComponent } from '../components/schedule-live-item/schedule-live-item';
import { NewsSlider } from '../components/news-slider/news-slider';
import { NewsItemComponent } from '../components/news-item/news-item';
import { StandingsComponent } from '../components/standings/standings-component';
import { PlayoffEncounterListComponent } from '../components/playoff-encounter-list/playoff-encounter-list';
import { PlayoffEncounterItemComponent } from '../components/playoff-encounter-item/playoff-encounter-item';
import { EncounterItemComponent } from '../components/encounter-item/encounter-item';
import { ButtonDashboardComponent } from '../components/button-dashboard/button-dashboard';
import { TeamLogoComponent } from '../components/team-logo/team-logo';
import { GameDetailsComponent } from '../components/game-details/game-details';
import { GameStatisticsComponent } from '../components/game-statistics/game-statistics';
import { GameLineupComponent } from '../components/game-lineup/game-lineup';
import { GameLineupItemComponent } from '../components/game-lineup-item/game-lineup-item';
import { CustomSpinnerComponent } from '../components/spinner/spinner';
import { ProgressbarSlimComponent } from '../components/progressbar-slim/progressbar-slim';
import { CustomImageComponent } from '../components/custom-image/custom-image';
import { CustomTableComponent } from '../components/custom-table/custom-table';
import { TeamDetailsComponent } from '../components/team-details/team-details';
import { TeamGameComponent } from '../components/team-game/team-game';
import { TeamRosterComponent } from '../components/team-roster/team-roster';
import { TeamStatisticsComponent } from '../components/team-statistics/team-statistics';

import { SafePipe } from '../pipes/safe/safe-pipe';
import { KeysPipe } from '../pipes/keys';

import { DashboardPage } from '../pages/dashboard/dashboard';
import { StandingsPage } from '../pages/standings/standings';
import { SchedulePage } from '../pages/schedule/schedule';
import { GamePage } from '../pages/game/game';
import { PlayoffPage } from '../pages/playoff/playoff';
import { NewsRoomPage } from '../pages/news/news-room';
import { VideoRoomPage } from '../pages/videos/video-room';
import { LeagueSettingsPage } from '../pages/league-settings/league-settings';
import { PlayerPage } from '../pages/player/player';
import { TeamPage } from '../pages/team/team';

@NgModule({
    declarations: [
        EishockeyLigaApp,
        DashboardPage,
        StandingsPage,
        SchedulePage,
        GamePage,
        PlayoffPage,
        VideoRoomPage,
        PlayerPage,
        LeagueSettingsPage,
        NewsRoomPage,
        TeamPage,
        TeamDetailsComponent,
        TeamGameComponent,
        TeamRosterComponent,
        TeamStatisticsComponent,
        ScheduleList,
        ScheduleItem,
        ScheduleLiveListComponent,
        ScheduleLiveItemComponent,
        StandingsComponent,
        PlayoffEncounterListComponent,
        PlayoffEncounterItemComponent,
        EncounterItemComponent,
        ButtonDashboardComponent,
        TeamLogoComponent,
        GameDetailsComponent,
        GameStatisticsComponent,
        GameLineupComponent,
        GameLineupItemComponent,
        CustomSpinnerComponent,
        ProgressbarSlimComponent,
        NewsItemComponent,
        CustomImageComponent,
        CustomTableComponent,
        NewsSlider,
        SafePipe,
        KeysPipe
    ],
    imports: [
        BrowserModule,
        HttpModule,
        JsonpModule,
        IonicModule.forRoot(EishockeyLigaApp, {
            preloadModules: true
        }),
        IonicStorageModule.forRoot(),
        ChartModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        EishockeyLigaApp,
        DashboardPage,
        StandingsPage,
        SchedulePage,
        GamePage,
        PlayerPage,
        PlayoffPage,
        VideoRoomPage,
        LeagueSettingsPage,
        NewsRoomPage,
        TeamPage
    ],
    providers: [
        DateHelper,
        PenaltiesHelper,
        HockeyDataConfigService,
        LeagueConfigService,
        StandingsService,
        ScheduleService,
        GameReportService,
        KnockoutService,
        YoutubeService,
        NewsService,
        PlayerService,
        TeamService,
        StatusBar,
        SplashScreen,
        InAppBrowser,
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler,
        }
    ]
})
export class AppModule {}
