import { concat } from 'lodash';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LeagueConfigService } from '../services/api';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { StandingsPage } from '../pages/standings/standings';
import { SchedulePage } from '../pages/schedule/schedule';
import { PlayoffPage } from '../pages/playoff/playoff';
import { VideoRoomPage } from '../pages/videos/video-room';
import { NewsRoomPage } from '../pages/news/news-room';
import { LeagueSettingsPage } from '../pages/league-settings/league-settings';
import { AboutPage } from '../pages/about/about';

@Component({
    templateUrl: 'menu.html'
})

export class EishockeyLigaApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = DashboardPage;
    activePage: any;
    menuSections: Array<{}>;
    private mainSections: Object;
    private settingsSections: Object;
    private leagueSections: Array<{}> = [];

    /**
     * @param {Platform} platform
     * @param {StatusBar} statusBar
     * @param {SplashScreen} splashScreen
     * @param {LeagueConfigService} leagueConfigService
     */
    constructor(
        public platform: Platform,
        public statusBar: StatusBar,
        public splashScreen: SplashScreen,
        private leagueConfigService: LeagueConfigService
    ) {
        this.initializeApp();

        // Listen to changes of the stored league settings.
        this.leagueConfigService.event.subscribe((leagues) => {
            this.buildLeagueSections(leagues);
            this.concatMenuSections();
        });
    }

    /**
     * Initialize the app when the platform and all plugins are available.
     * Do any higher level native things here.
     */
    initializeApp() {
        this.platform.ready().then(() => {
            // Hide splash screen.
            this.splashScreen.hide();

            // Let status bar overlay webview.
            // this.statusBar.overlaysWebView(true);
            this.statusBar.overlaysWebView(false);

            // Set status bar to light.
            this.statusBar.styleLightContent();

            // Set status bar to white.
            this.statusBar.backgroundColorByHexString('#222838');

            // Fetch the current league settings.
            this.leagueConfigService.getSettings();

            // Build the menu.
            this.buildMainSections();
            this.buildSettingsSections();
            this.concatMenuSections();
        });
    }

    /**
     * Open a single page.
     *
     * Reset the content nav to have just this page
     * we wouldn't want the back button to show in this scenario
     *
     * @param {Object} page
     */
    openPage(page) {
        this.nav.setRoot(page.component, {
            'league': page.league
        });
        this.activePage = page;
    }

    /**
     * Opens the dashboard page.
     */
    openDashboard() {
        this.nav.setRoot(DashboardPage);
        this.activePage = DashboardPage;
    }

    /**
     * Toggle a menu section to be opened or closed.
     *
     * @param {Object} page
     */
    toggleMenuSection(page) {
        page.isOpen = !page.isOpen;
    }

    /**
     * Combines all menu sections to the final menu section structure.
     */
    private concatMenuSections() {
        this.menuSections = concat([], this.mainSections, this.leagueSections, this.settingsSections);
    }

    /**
     * Builds the main sections of the menu.
     */
    private buildMainSections() {
        let dashboard = {
            title: 'Dashboard',
            isVisible: true,
            isOpen: false,
            children: null,
            component: DashboardPage,
            icon: 'icon-dashboard',
            isBeta: false
        };

        let videoRoom = {
            title: 'Video Room',
            isVisible: true,
            isOpen: false,
            children: null,
            component: VideoRoomPage,
            icon: 'icon-video',
            isBeta: true
        };

        let newsRoom = {
            title: 'News Room',
            isVisible: true,
            isOpen: false,
            children: null,
            component: NewsRoomPage,
            icon: 'icon-news',
            isBeta: true
        };

        this.mainSections = {
            sectionHeader: '',
            isVisible: false,
            pages: [
                dashboard,
                videoRoom,
                newsRoom
            ]
        };
    }

    /**
     * Builds the settings section of the menu.
     */
    private buildSettingsSections() {
        let leagueSettings = {
            title: 'Liga Einstellungen',
            isVisible: true,
            isOpen: false,
            children: null,
            component: LeagueSettingsPage,
            icon: 'icon-settings',
            isBeta: false
        };

        let about = {
            title: 'Infos',
            isVisible: true,
            isOpen: false,
            children: null,
            component: AboutPage,
            icon: 'icon-info',
            isBeta: false
        };

        this.settingsSections = {
            sectionHeader: 'Einstellungen',
            isVisible: true,
            pages: [
                leagueSettings,
                about
            ]
        };
    }

    /**
     * Builds the leagues section of the menu.
     *
     * @param {Array} leagueGroups
     */
    private buildLeagueSections(leagueGroups) {
        this.leagueSections = [];

        if (!leagueGroups || !leagueGroups.length) {
            return false;
        }

        let section;
        let page;

        for (let region of leagueGroups) {
            if (!region.leagues.length) {
                continue;
            }

            section = {
                sectionHeader: region.title || '',
                isVisible: false,
                pages: []
            };

            for (let league of region.leagues) {
                if (!league.isActive) {
                    continue;
                }

                page = this.retrieveLeagueMenuItem(league);

                section.isVisible = true;
                section.pages.push(page);
            }

            this.leagueSections.push(section);
        }
    }

    /**
     * Returns a league menu item.
     *
     * @param {Object} league
     */
    private retrieveLeagueMenuItem(league) {
        let page = {
            title: league.title || '',
            isOpen: false,
            children: [],
            icon: '',
            isBeta: false
        };

        page.children.push(
            {
                title: 'Tabelle',
                icon: 'icon-table',
                component: StandingsPage,
                league: league
            }
        );

        page.children.push(
            {
                title: 'Spielplan',
                icon: 'icon-schedule',
                component: SchedulePage,
                league: league
            }
        );

        if (league.playoff !== null) {
            page.children.push({
                title: 'Playoff',
                icon: 'icon-playoff',
                component: PlayoffPage,
                league: league
            });
        }

        return page;
    }
}
