import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HockeyDataConfigService } from './hockey-data-config';
import { BaseApiService } from './base-service';

@Injectable()
export class ScheduleService extends BaseApiService {

    /**
     * @param {Jsonp} Jsonp
     * @param {HockeyDataConfigService} hockeyDataConfig
     */
    constructor(
        public jsonp: Jsonp,
        private hockeyDataConfig: HockeyDataConfigService
    ) {
        super();
    }

    /**
     * Fetches the schedule api and returnes a clustered games list by their game date.
     *
     * @param {Number} leagueId
     *
     * return {Observable}
     */
    retrieveGroupedGames(leagueId): Observable<Object> {
        let url = this.hockeyDataConfig.retrieveScheduleUrl(leagueId);

        return this.jsonp
            .get(url)
            .map(this.extractData.bind(this));
    }

    /**
     * Fetches the games of the day.
     *
     * @param {Number} leagueId
     *
     * return {Observable}
     */
    retrieveTodayGames(leagueId): Observable<Object> {
        let url = this.hockeyDataConfig.retrieveScheduleTodayUrl(leagueId);

        return this.jsonp
            .get(url)
            .map(this.extractData.bind(this));
    }

    /**
     * Fetches the upcoming games.
     *
     * @param {Number} leagueId
     *
     * return {Observable}
     */
    retrieveNextGames(leagueId): Observable<Object> {
        let url = this.hockeyDataConfig.retrieveScheduleNextUrl(leagueId);

        return this.jsonp
            .get(url)
            .map(this.extractData.bind(this));
    }

    /**
     * Fetches the latest played games.
     *
     * @param {Number} leagueId
     *
     * return {Observable}
     */
    retrieveLatestGames(leagueId): Observable<Object> {
        let url = this.hockeyDataConfig.retrieveScheduleLatestUrl(leagueId);

        return this.jsonp
            .get(url)
            .map(this.extractData.bind(this));
    }
}
