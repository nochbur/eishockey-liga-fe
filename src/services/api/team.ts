import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HockeyDataConfigService } from './hockey-data-config';
import { BaseApiService } from './base-service';

@Injectable()
export class TeamService extends BaseApiService {
    /**
     * @param {Jsonp} Jsonp
     * @param {HockeyDataConfigService} hockeyDataConfig
     */
    constructor(
        public jsonp: Jsonp,
        private hockeyDataConfig: HockeyDataConfigService
    ) {
        super();
    }

    /**
     * Fetches the team detail page.
     *
     * @param {Number} leagueId
     * @param {Number} teamId
     *
     * return {Observable}
     */
    retrieveTeamDetails(leagueId, teamId): Observable<{}> {
        let url = this.hockeyDataConfig.retrieveTeamDetailUrl(leagueId, teamId);

        return this.jsonp
            .get(url)
            .map(this.extractData);
    }
}
