import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HockeyDataConfigService } from './hockey-data-config';
import { BaseApiService } from './base-service';

@Injectable()
export class KnockoutService extends BaseApiService {

    /**
     * @param {Jsonp} Jsonp
     * @param {HockeyDataConfigService} hockeyDataConfig
     */
    constructor(
        public jsonp: Jsonp,
        private hockeyDataConfig: HockeyDataConfigService
    ) {
        super();
    }

    /**
     * Fetches the knockout data for a given league id.
     *
     * @param {Number} leagueId
     *
     * return {Observable}
     */
    retrieveKnockout(leagueId): Observable<Array<{}>> {
        let url = this.hockeyDataConfig.retrieveKnockoutUrl(leagueId);

        return this.jsonp
            .get(url)
            .map(this.extractData);
    }
}
