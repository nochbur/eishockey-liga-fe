import { Response } from '@angular/http';

export class BaseApiService {
    /**
     * Returns the data property of a given response.
     *
     * @param {Response} response
     *
     * @return {Object|null}
     */
    extractData(response: Response) {
        let body = response.json();

        return body.data || null;
    }

    /**
     * Returns the json body of a given response.
     *
     * @param {Response} response
     *
     * @return {Object|null}
     */
    retrieveData(response: Response) {
        return response.json() || null;
    }
}
