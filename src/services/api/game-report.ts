import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HockeyDataConfigService } from './hockey-data-config';
import { BaseApiService } from './base-service';

@Injectable()
export class GameReportService extends BaseApiService {

    /**
     * @param {Jsonp} Jsonp
     * @param {HockeyDataConfigService} hockeyDataConfig
     */
    constructor(
        public jsonp: Jsonp,
        private hockeyDataConfig: HockeyDataConfigService
    ) {
        super();
    }

    /**
     * Fetches the game report for a given id.
     *
     * @param {Number} leagueId
     * @param {Number} gameId
     *
     * return {Observable}
     */
    retrieveGameReport(leagueId, gameId): Observable<{}> {
        let url = this.hockeyDataConfig.retrieveGameReportUrl(leagueId, gameId);

        return this.jsonp
            .get(url)
            .map(this.extractData);
    }
}
