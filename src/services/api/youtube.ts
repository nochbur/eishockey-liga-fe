import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BaseApiService } from './base-service';

@Injectable()
export class YoutubeService extends BaseApiService {
    private BASE_URL = 'https://www.googleapis.com/youtube/v3';
    private API_KEY = 'AIzaSyDdKmaHRxqc5oRNGdc9fzVc2lU3my6tlo0';

    /**
     * @param {Http} http
     */
    constructor(
        private http: Http
    ) {
        super();
    }

    /**
     * Fetches all videos from youtube.
     *
     * @param {Object} params
     *
     * return {Observable}
     */
    fetchPlaylist(params): Observable<Object> {
        let url = this.retrieveYoutubePlaylistUrl(params);

        return this.http
            .get(url)
            .map(this.retrieveData.bind(this));
    }

    /**
     * Returns a youtube url with all needed params such as api key.
     *
     * @param {Object} params
     *
     * return {String}
     */
    retrieveYoutubePlaylistUrl(params) {
        let urlParams = new URLSearchParams();
        let defaultParams = {
            'part': 'snippet',
            'playlistId': null,
            'pageToken': null,
            'maxResults': 10,
            'key': this.API_KEY
        }

        for(let key in defaultParams){
            urlParams.set(key, defaultParams[key]);
        }

        for(let key in params){
            urlParams.set(key, params[key]);
        }

        return this.BASE_URL + '/playlistItems?' + urlParams.toString();
    }
}
