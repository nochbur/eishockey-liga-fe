import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HockeyDataConfigService } from './hockey-data-config';
import { BaseApiService } from './base-service';

@Injectable()
export class PlayerService extends BaseApiService {

    /**
     * Constructor.
     */
    constructor(
        public jsonp: Jsonp,
        private hockeyDataConfig: HockeyDataConfigService
    ) {
        super();
    }

    /**
     * Fetches the details for a given playerId.
     *
     * @param {Number} leagueId
     * @param {Number} playerId
     *
     * return {Observable}
     */
    retrievePlayerData(leagueId, playerId): Observable<{}> {
        let url = this.hockeyDataConfig.retrievePlayerUrl(leagueId, playerId);

        return this.jsonp
            .get(url)
            .map(this.extractData);
    }
}
