import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HockeyDataConfigService } from './hockey-data-config';
import { BaseApiService } from './base-service';

@Injectable()
export class StandingsService extends BaseApiService {

    /**
     * @param {Jsonp} Jsonp
     * @param {HockeyDataConfigService} hockeyDataConfig
     */
    constructor(
        public jsonp: Jsonp,
        private hockeyDataConfig: HockeyDataConfigService
    ) {
        super();
    }

    /**
     * Fetches the league standings.
     *
     * @param {Number} leagueId
     *
     * return {Observable}
     */
    retrieveStandings(leagueId: number): Observable<{}> {
        let url = this.hockeyDataConfig.retrieveStandingsUrl(leagueId);

        return this.jsonp
            .get(url)
            .map(this.extractData);
    }
}
