import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { Jsonp } from '@angular/http';
import { Storage } from '@ionic/storage';
import { HockeyDataConfigService } from './hockey-data-config';
import { BaseApiService } from './base-service';

@Injectable()
export class LeagueConfigService extends BaseApiService {

    public leagues = new Subject<object>();
    public event = this.leagues.asObservable();
    private storedLeagues: Array<string> = [];
    private LEAGUE_STORAGE_KEY = 'eishockeyLiga.storedLeagues';

    /**
     * @param {jsonp} Jsonp
     * @param {hockeyDataConfig} HockeyDataConfigService
     * @param {storage} Storage
     */
    constructor(
        public jsonp: Jsonp,
        private hockeyDataConfig: HockeyDataConfigService,
        public storage: Storage
    ) {
        super();
    }

    /**
     * Checks if there are already stored leagues and triggers then the sync process.
     */
    getSettings() {
        this.storage.get(this.LEAGUE_STORAGE_KEY)
            .then(
                (leagues) => {
                    this.storedLeagues = leagues || [];
                    this.syncStorageFromApi();
                },
                (error) => {
                    console.error('An error occured while fetching stored league settings.', error);
                    this.storedLeagues = [];
                    this.syncStorageFromApi();
                },
            );
    }

    /**
     * Stores a given array of leagues to the local storage.
     *
     * @param {Array} leagueGroups
     * @param {Boolean} updateFromStorage
     */
    setStoredLeagues(leagueGroups, updateFromStorage) {
        let leagueIds = [];

        // Check if a league should be active or not.
        for (let group of leagueGroups) {
            for (let league of group.leagues) {
                if (league.isActive === true) {
                    leagueIds.push(league.id);

                    continue;
                }

                // If the current league is not already in the list and if the list should be updated
                // by the api then the leauge is activated.
                if (this.storedLeagues.indexOf(league.id) != -1 && updateFromStorage) {
                    league.isActive = true;
                    leagueIds.push(league.id);
                }
            }
        }

        this.storage.set(this.LEAGUE_STORAGE_KEY, leagueIds);
        this.storedLeagues = leagueIds;
        // Triggers the service event listener.
        this.leagues.next(leagueGroups);
    }

    /**
     * Triggers the api call to get the lastest league settings.
     */
    private syncStorageFromApi() {
        this.fetchSettings()
            .subscribe(
                data => this.onApiFetched(data, null),
                error => this.onApiFetched(null, error)
            );
    }

    /**
     * Fetches the league settings.
     *
     * return {Observable}
     */
    private fetchSettings(): Observable<Array<{}>> {
        let url = this.hockeyDataConfig.retrieveLeaguesUrl();

        return this.jsonp
            .get(url)
            .map(this.extractData);
    }

    /**
     * Triggered once the api call is completed.
     *
     * @param {Array} data
     * @param {Object} error
     */
    private onApiFetched(data, error) {
        // If there was a problem start the workflow again.
        // This could lead to a recusion if the api is down.
        if (error) {
            console.error('Error while fetching from leagues api.', error);
            this.syncStorageFromApi();

            return false;
        }

        this.setStoredLeagues(data, true);
    }
}
