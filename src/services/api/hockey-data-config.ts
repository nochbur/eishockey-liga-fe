import { Injectable } from '@angular/core';

@Injectable()
export class HockeyDataConfigService {

    private baseUrl = 'https://api.eishockey-liga.at';
    // private baseUrl = 'http://localhost:6543';

    /**
     * Returns the base url.
     *
     * @return {String}
     */
    retrieveApiBaseUrl() {
        return this.baseUrl;
    }

    /**
     * Returns the leagues base url.
     *
     * @return {String}
     */
    retrieveLeaguesUrl() {
        let url = this.baseUrl + '/leagues';

        return this.retrieveSimpleJsonpUrl(url);
    }

    /**
     * Returns the standing url for a given league id.
     *
     * @param {Number} leagueId
     *
     * @return {String}
     */
    retrieveStandingsUrl(leagueId) {
        let url = this.baseUrl + '/leagues/' + leagueId + '/standings';

        return this.retrieveSimpleJsonpUrl(url);
    }

    /**
     * Returns the schedule url for a given league id.
     *
     * @param {Number} leagueId
     *
     * @return {String}
     */
    retrieveScheduleUrl(leagueId) {
        let url = this.baseUrl + '/leagues/' + leagueId + '/schedule';

        return this.retrieveSimpleJsonpUrl(url);
    }

    /**
     * Returns the todays schedule url for a given league id.
     *
     * @param {Number} leagueId
     *
     * @return {String}
     */
    retrieveScheduleTodayUrl(leagueId) {
        let url = this.baseUrl + '/leagues/' + leagueId + '/schedule-today';

        return this.retrieveSimpleJsonpUrl(url);
    }

    /**
     * Returns the next schedule url for a given league id.
     *
     * @param {Number} leagueId
     *
     * @return {String}
     */
    retrieveScheduleNextUrl(leagueId) {
        let url = this.baseUrl + '/leagues/' + leagueId + '/schedule-next';

        return this.retrieveSimpleJsonpUrl(url);
    }

    /**
     * Returns the latest schedule url for a given league id.
     *
     * @param {Number} leagueId
     *
     * @return {String}
     */
    retrieveScheduleLatestUrl(leagueId) {
        let url = this.baseUrl + '/leagues/' + leagueId + '/schedule-latest';

        return this.retrieveSimpleJsonpUrl(url);
    }

    /**
     * Returns the knockout url for a given league id.
     *
     * @param {Number} leagueId
     *
     * @return {String}
     */
    retrieveKnockoutUrl(leagueId) {
        let url = this.baseUrl + '/leagues/' + leagueId + '/knockout';

        return this.retrieveSimpleJsonpUrl(url);
    }

    /**
     * Returns the game report url for a given league id and game id.
     *
     * @param {Number} leagueId
     * @param {Number} gameId
     *
     * @return {String}
     */
    retrieveGameReportUrl(leagueId, gameId) {
        let url = this.baseUrl + '/leagues/' + leagueId + '/game/' + gameId;

        return this.retrieveSimpleJsonpUrl(url);
    }

    /**
     * Returns the team detail page url for a given league id and team id.
     *
     * @param {Number} leagueId
     * @param {Number} teamId
     *
     * @return {String}
     */
    retrieveTeamDetailUrl(leagueId, teamId) {
        let url = this.baseUrl + '/leagues/' + leagueId + '/teams/' + teamId;

        return this.retrieveSimpleJsonpUrl(url);
    }

    /**
     * Returns the game report url for a given league id and game id.
     *
     * @return {String}
     */
    retrieveNewsUrl() {
        let url = this.baseUrl + '/news';

        return this.retrieveSimpleJsonpUrl(url);
    }

    /**
     * Returns the member url for a given league and player id.
     *
     * @param {Number} leagueId
     * @param {Number} playerId
     *
     * @return {String}
     */
    retrievePlayerUrl(leagueId, playerId) {
        let url = this.baseUrl + '/leagues/' + leagueId + '/players/' + playerId;

        return this.retrieveSimpleJsonpUrl(url);
    }

    /**
     * Returns an url with a jsonp callback.
     *
     * @param {String} url
     *
     * @return {String}
     */
    private retrieveSimpleJsonpUrl(url: string) {
        return url + '?callback=JSONP_CALLBACK'
    }
}
