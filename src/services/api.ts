import { GameReportService } from './api/game-report';
import { HockeyDataConfigService } from './api/hockey-data-config';
import { KnockoutService } from './api/knockout';
import { LeagueConfigService } from './api/league-config';
import { NewsService } from './api/news';
import { PlayerService } from './api/player';
import { ScheduleService } from './api/schedule';
import { StandingsService } from './api/standings';
import { TeamService } from './api/team';
import { YoutubeService } from './api/youtube';

export {
    GameReportService,
    HockeyDataConfigService,
    KnockoutService,
    LeagueConfigService,
    NewsService,
    PlayerService,
    ScheduleService,
    StandingsService,
    TeamService,
    YoutubeService
};