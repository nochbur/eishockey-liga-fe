# Eishockey Liga Components Guide

This is a set of documentation for specific components.

## Custom Table Component

### General:
- Class: CustomTableComponent
- Selector: custom-table
- TemplateUrl: custom-table.html

Description:
This component can be used to dynamically build and fill a table in the style of the app.
The amount and the width of the columns can be set easily. As well as an optional title block.
The data for the lines/rows can be passed as arrays.

### HTML

Snippet of the HTML used to include the component (as present on the Player page):
```
	<custom-table *ngIf="overviewLines"
				  [title]="'Player Details'"
				  [lines]="overviewLines"
				  [columnNames]="overviewColumnNames"
				  [columnWidth]="overviewColumnWidth"
				  [hasSeparator]="true">
	</custom-table>
```

### Parameters

The following parameters are used by this component:

1. Title
```
[title]="'Player Details'"
```
Type: String
Optional: Yes

If not present, no headline block will be displayed.

2. Lines
```
[lines]="overviewLines"
```
Type: Array<Array<String|Number>>
Optional: No

If not present, no list will be displayed.

3. ColumnNames
```
[columnNames]="overviewColumnNames"
```
Type: Array<String>
Optional: No

Names for the Columns of the table. Can be empty string, but must be present. All combinations will be accepted.
Examples:
- ['Nr', 'Name', 'Score']
- ['', '', 'Score']

4. ColumnWidth
```
[columnWidth]="overviewColumnWidth"
```
Type: Array<Number>
Optional: No

An array of Numbers to define the width and the amount of the columns.
If [1, 4, 7] is given, a table with the following columns will be created: `col-1`, `col-4` and `col-7`.
IMPORTANT: The length of the columnNames and the columnWidth needs to match!

5. HasSeparator
```
[hasSeparator]="true"
```
Type: Boolean
Optional: Yes

Defaults to `true`. Controls if there is a line separating two lines from each other.

## CustomImageComponent

### General:
- Class: CustomImageComponent
- Selector: custom-image
- TemplateUrl: custom-image.html

Description:
An image wrapper that includes a fallback image if no src is provided, if a src is given but invalid
the fallback image will be displayed instead. Stylable outside of the component.

### HTML

Snippet of the HTML used to include the component (as present on the Player page):
```
    <custom-image [src]="playerData.images.playerSmallPortrait"></custom-image>
```

### Parameters

The following parameter is used by this component:

1. Src
```
[src]="playerData.images.playerSmallPortrait"
```
Type: String
Optional: Yes, but recommended

An absolute URL. If not present, a placeholder image will be displayed.
