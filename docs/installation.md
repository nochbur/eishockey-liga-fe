# Eishockey Liga Installation Guide

This app is based on the [ionic framework](http://ionicframework.com) which uses [angular 2](https://angular.io/).

## Requirements

We recommend to use homebrew to install the dependencies: [How to install homebrew](https://brew.sh/index_de.html)

### Environment

- node
- npm
- ionic
- cordova

```
brew install npm node
npm install -g ionic cordova
```

## Project setup

### Clone the project

```
git clone git@bitbucket.org:nochbur/eishockey-liga-fe.git
```

### Install the project

```
cd eishockey-liga-fe/
npm install
```

### Add plattform iOS

```
ionic cordova platform add ios
```

### Add plattform android

```
ionic cordova platform add android
```

## Start the project

### Open in browser

```
ionic serve
```

### Run in iOS simulator

Make sure you have a SDK installed and the path correctly exported.

```
ionic run ios -l
```

### Run in android simulator

Make sure you have a SDK installed and the path correctly exported.

```
ionic run android -l
```

### Run on an android device

To view and debug the project within an Android SDK using a android device:

```
ionic cordova run android
```

Important: If the command fails with an exception addressing the license agreement, make sure the respective
Android SDK version is enabled within Android Studio.

To enable the necessary SDK platform (in this example platform 24):
- Open Android Studio.
- Open Android Studio Menu > Preferences.
- Appearance & Behavior > System Settings > Android SDK
- Tick the box for Android 7.0 (Nougat) | 24            <----- Necessary SDK platform here.
- Click ok and follow the instructions.
