# Eishockey Liga Deployment Guide

## 1 Stage deployment

Follow this checklist:

- `git checkout develop`
- `git pull`
- `git checkout -b rc/<version>`
- Update the CHANGES.md file and add the new version as header.
- Add and commit your changes.
- Publish the branch.
- `ssh pna@88.99.169.109`
- `cd /var/data/websites/stage.eishockey-liga.at`
- `git fetch origin`
- `git checkout rc/<version>`
