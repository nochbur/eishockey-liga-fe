# Eishockey Liga Development Guide

## 1. Add a new page

Add a new page in the pages directory:

```
mkdir eishockey-liga-fe/src/pages/<page-name>
touch eishockey-liga-fe/src/pages/<page-name>/<controller>.ts
touch eishockey-liga-fe/src/pages/<page-name>/<template>.html
touch eishockey-liga-fe/src/pages/<page-name>/<styles>.scss
```

Your page must be registered in the `app.modules` file.

```
vi eishockey-liga-fe/src/app/app.modules.ts
```

Here is an example:

```
import { SamplePage } from '../pages/sample/my-sample';

@NgModule({
    declarations: [
        SamplePage
    ],
    ...
    entryComponents: [
        SamplePage
    ]
})
```

## 2. Add a new service

Add a new service in the services directory:

```
touch eishockey-liga-fe/src/services/<service>.ts
```

Your service must be registered in the `app.modules` file.

```
vi eishockey-liga-fe/src/app/app.modules.ts
```

Here is an example:

```
import { SampleService } from '../services/sample';

@NgModule({
    ...
    providers: [
        SamplePage
    ]
})
```

## 3. Add a new component

Add a new component in the component directory:

```
mkdir eishockey-liga-fe/src/components/<component-name>
touch eishockey-liga-fe/src/components/<component-name>/<controller>.ts
touch eishockey-liga-fe/src/components/<component-name>/<template>.html
touch eishockey-liga-fe/src/components/<component-name>/<styles>.scss
```

Your component must be registered in the `app.modules` file.

```
vi eishockey-liga-fe/src/app/app.modules.ts
```

Here is an example:

```
import { SampleComponent } from '../components/sample/sample-component';

@NgModule({
    declarations: [
        SampleComponent
    ]
})
```

## 4. Build the static files of the app

### 4.1 Compile

This should be part of every new feature or change of the source code. Run this script before merging into the develop/release branch.

```
ionic build --prod
```

### Build for iOS

Run this if you need to update the builded iOS app. (4.1 must also be executed before this)

```
ionic build ios --release
```

### Build for android

Run this if you need to update the builded android app. (4.1 must also be executed before this)

```
ionic build android --release
```

## 5. Ionic CLI commands

A detailed list can be found [here](https://ionicframework.com/docs/cli/commands.html).
