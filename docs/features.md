# Eishockey Liga Feature List

**Content**

- [1. Dashboard](#1-dashboard)
- [2. Video Room](#2-video-room)
- [3. News Room](#3-news-room)
- [4. League](#4-league)
    - [4.1 League Standings](#41-league-standings)
    - [4.2 League Schedule](#42-league-schedule)
    - [4.3 League Playoff](#43-league-playoff)
- [5. Game Detail Page](#5-game-detail-page)
    - [5.1 Game Details](#51-game-details)
    - [5.2 Game Statistic](#52-game-statistic)
    - [5.3 Game Lineup](#53-game-lineup)
- [6. League Settings](#6-league-settings)

## 1. Dashboard

TODO

## 2. Video Room

TODO

## 3. News Room

TODO

## 4. League

TODO

### 4.1 League Standings

TODO

### 4.2 League Schedule

TODO

### 4.3 League Playoff

TODO

## 5. Game Detail Page

TODO

### 5.1 Game Details

TODO

### 5.2 Game Statistic

TODO

### 5.3 Game Lineup

TODO

## 6. League Settings

TODO
