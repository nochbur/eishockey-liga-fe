# Eishockey-Liga-Fe

Eishockey-Liga-Fe is an ionic project. [ionicframework.com](https://ionicframework.com)

## Short description

This app displays the ice hockey leagues of Austria. It shows the leagues standings and shedules as well as a detailed game report.

## Stage Server

- Front end browser Version: [test.eishockey-liga.at](http://test.eishockey-liga.at)

## Documentation

- [Features](docs/features.md)
- [Installation](docs/installation.md)
- [Development](docs/development.md)
- [Deployment](docs/deployment.md)
- [Components](docs/components.md)
