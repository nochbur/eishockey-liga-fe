# Changes Eishockey Liga FE

##### unreleased

- ENHANCEMENT: Updated to latest ionic version and added ionic pro.

##### 2017-12-05 0.1.2

- BUGFIX: Fixed bug on team page last games.
- FEATURE: Changed icons and refactored menu.
- FEATURE: Added player stats to player page.

##### 2017-11-04 0.1.1

- ENHANCEMENT: Added team icon to standings.
- BUGFIX: Fixed fallback for player stats.
- BUGFIX: Fixed gameday endless loading spinner.
- BUGFIX: Fixed player fallback image.
- BUGFIX: Fixed dashboard button on each sub page.
- ENHANCEMENT: Added ios and android plattform.
  Development:
    rm -rf node_modules
    npm install

##### 2017-11-04 0.1.0

- ENHANCEMENT: Updated ionic framework and brought the services in a modern structure.
- FEATURE: Fixed player page style and added links to it.
- FEATURE: Added Player page and custom-table component. #9
- ENHANCEMENT: Added game-lineup-item component and refactored game-linup to use new component. #9
- FEATURE: Updated splash screens.
- FEATURE: Added team page. #59
  Development:
    rm -rf node_modules
    npm install

- ENHANCEMENT: Removed unused service worker.
- ENHANCEMENT: Added penalties mapping for game details. #75
- BUGFIX: Fixed bug on schedule list where game of the day is not showing the start time but an empty result. #74
- FEATURE: Split all colors up and moved them into a color-schema file. #51
- FEATURE: User league selections are stored in the local storage. #49
- BUGFIX: Fixed bug on game detail page when game has not started yet. #73
- ENHANCEMENT: Updated documentation.
- ENHANCEMENT: Updated CHANGES.txt to be a md file.
- ENHANCEMENT: Renamed repository on bitbucket.

##### 2017-01-16 0.0.0

- [FEATURE] Added dummy about page
- [FEATURE] Added video corner
- [FEATURE] Added news corner
- [FEATURE] Added game details page
- [FEATURE] Added standings page
- [FEATURE] Added schedule page
- [BUGFIX] Fixed bug on dashboard.
- [FEATURE] Added Dashboard
- [FEATURE] Added basic menu structure
- [FEATURE] Init commit
